/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CMAINMODEL_H
#define CMAINMODEL_H

#include "datamodel_global.h"

#include <QAbstractItemModel>
#include <QMutex>

#include "IModel.h"
#include "CSessionItem.h"

namespace Overhowl {
namespace DataModel {

class DATAMODELSHARED_EXPORT CMainModel : public QAbstractItemModel, public IModel
{
  friend class CSerializer;
  Q_OBJECT

private:
  CMainModel(QObject* parent = nullptr);

public:
  static CMainModel& Instance(QObject* parent = nullptr);

  // QAbstractItemModel
  bool canDropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex & parent) const override;
  int columnCount(const QModelIndex &parent) const override;
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild) override;
  QModelIndex parent(const QModelIndex &child) const override;
  bool removeColumns(int column, int count, const QModelIndex& parent = QModelIndex()) override;
  bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;
  QModelIndex index(int row, int column, const QModelIndex &parent) const override;
  Qt::ItemFlags flags(const QModelIndex &index) const override;
  QVariant data(const QModelIndex &index, int role) const override;
  bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  QMimeData* mimeData(const QModelIndexList& indexes) const override;
  QStringList mimeTypes() const override;
  Qt::DropActions supportedDropActions() const override;

  void AddItem(QString sName, CSessionItem::EType eType, int iParentID);
  bool Modified();
  void Clear();
  void DataSaved();
  void SetModified();
  CSessionItem::EType TypeForItem(int iID);
  QString FilenameForItem(int iID);
  bool DuplicateCheck(int iID, QString sName);


signals:
  void SignalModified();


public slots:
  void ModifiedSlot();


private slots:
  void DataChangedSlot(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles = QVector<int> ());
  void DataInserted(const QModelIndex &parent, int first, int last);
  void DataMoved(const QModelIndex &parent, int start, int end, const QModelIndex &destination, int where);
  void DataRemoved(const QModelIndex &parent, int first, int last);


private:
  void AddSession(CSessionItem* session);
  void AddItem(CSessionItem* item, CSessionItem* parent);
  void Lock();
  void Unlock();
  CSessionItem* FindItem(int iID) const;
  CSessionItem* FindItem(CSessionItem* item, int iID, bool& bRemove) const;
  CSessionItem* RemoveItem(int iID) const;
  CSessionItem* GetSessionItem();
  CSessionItem* GetRootItem()
  {
    return m_rootItem;
  }


private:
  static CMainModel*          m_instance;
  CSessionItem*               m_rootItem = nullptr;

  bool                        m_bModified = false;

  mutable QMutex              m_lock;

  const QString               m_sMimeType = "overhowl/item";
};

}
}

#endif // CMAINMODEL_H
