/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CItemFactory.h"
#include "CItemScript.h"
#include "CItemProject.h"
#include "CItemFolder.h"

namespace Overhowl {
namespace DataModel {

CItemFactory::CItemFactory()
{

}

CSessionItem* CItemFactory::CreateItem(QString sName, CSessionItem::EType eType, CSessionItem* parent, long iID)
{
  CSessionItem* item = nullptr;
  switch (eType)
  {
  case CSessionItem::Session:
    item = new CSessionItem(sName, CSessionItem::Session, parent);
    break;

  case CSessionItem::Script:
    item = new CItemScript(sName, parent);
    break;

  case CSessionItem::Project:
    item = new CItemProject(sName, parent);
    break;

  case CSessionItem::Folder:
    item = new CItemFolder(sName, parent);
    break;

  default:
    item = nullptr;
  }

  if (item != nullptr)
  {
    item->SetID(iID);
  }

  return item;
}

CSessionItem* CItemFactory::CreateTopLevelItem(QString sName)
{
  return new CSessionItem(sName, CSessionItem::None);
}

}
}
