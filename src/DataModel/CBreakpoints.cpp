/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CBreakpoints.h"
#include "IEngineController.h"

namespace Overhowl {
namespace DataModel {

CBreakpoints::CBreakpoints(QObject* parent)
  : QAbstractTableModel(parent)
{

}

void CBreakpoints::SetEngineController(IEngineController* engine)
{
  m_engine = engine;
}

void CBreakpoints::ToggleBreakpoint(const QString& sFilename, int iLineNumber)
{
  int i = 0;
  for (SBreakpoint sBreakpoint : m_liBreakpoints)
  {
    if (sBreakpoint.m_sFilename == sFilename && sBreakpoint.m_iLineNumber == iLineNumber)
    {
      //// breakpoint already in list, set it to active
      //sBreakpoint.m_bActive = true;

      beginRemoveRows(QModelIndex(), rowCount() - 1, rowCount() - 1);
      m_liBreakpoints.remove(i);
      if (m_engine)
      {
        m_engine->RemoveBreakpoint(SBreakpoint{sFilename, iLineNumber});
      }
      endRemoveRows();

      return;
    }
    ++i;
  }

  beginInsertRows(QModelIndex(), rowCount(), rowCount());
  // add breaktpoint if not available
  m_liBreakpoints.append({ sFilename, iLineNumber, true });
  if (m_engine)
  {
    m_engine->AddBreakpoint(SBreakpoint{sFilename, iLineNumber});
  }
  endInsertRows();
}

QVector<SBreakpoint> CBreakpoints::GetBreakpoints(void)
{
  return m_liBreakpoints;
}

bool CBreakpoints::IsBreakpoint(const QString& sFilename, int iLineNumber)
{
  for (SBreakpoint sBreakpoint : m_liBreakpoints)
  {
    if (sBreakpoint.m_sFilename == sFilename && sBreakpoint.m_iLineNumber == iLineNumber)
    {
      return true;
    }
  }

  return false;
}

int CBreakpoints::rowCount(const QModelIndex& /*parent*/) const
{
  return m_liBreakpoints.size();
}

int CBreakpoints::columnCount(const QModelIndex& /*parent*/) const
{
  return COLUMNS;
}

QModelIndex CBreakpoints::parent(const QModelIndex& /*child*/) const
{
  return QModelIndex();
}

Qt::ItemFlags CBreakpoints::flags(const QModelIndex& index) const
{
  if (!index.isValid())
  {
    return Qt::NoItemFlags;
  }

  if (index.column() == ACTIVE)
  {
    return QAbstractTableModel::flags(index) | Qt::ItemIsUserCheckable;
  }

  return QAbstractTableModel::flags(index);
}

QVariant CBreakpoints::data(const QModelIndex& index, int role) const
{
  if (!index.isValid() || index.row() >= m_liBreakpoints.size())
  {
    return QVariant();
  }

  if (role == Qt::DisplayRole)
  {
    const SBreakpoint& breakpoint = m_liBreakpoints.at(index.row());
    switch(index.column())
    {
    case LINE:
      return QVariant(breakpoint.m_iLineNumber);
    break;

    case FILE:
      return QVariant(breakpoint.m_sFilename.split("/").last());
    break;

    default:
      break;
    }
  }
  else if (role == Qt::CheckStateRole)
  {
    if (index.column() == ACTIVE)
    {
      return (m_liBreakpoints.at(index.column()).m_bActive ? Qt::Checked : Qt::Unchecked);
    }
  }
  else if (role == Qt::TextAlignmentRole)
  {
    if (index.column() == LINE)
    {
      return Qt::AlignCenter;
    }
  }
  else if (role == Qt::ToolTipRole)
  {
    if (index.column() == FILE)
    {
      return m_liBreakpoints.at(index.row()).m_sFilename;
    }
  }

  return QVariant();
}

QVariant CBreakpoints::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
  {
    switch (section)
    {
    case ACTIVE:
      //return "Active";
      break;

    case LINE:
      return "  Line  ";
      break;

    case FILE:
      return "File";
      break;
    }
  }

  return QVariant();
}

bool CBreakpoints::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (!index.isValid())
  {
    return false;
  }

  if (role == Qt::CheckStateRole)
  {
    if (index.column() == ACTIVE)
    {
      m_liBreakpoints[index.column()].m_bActive = ((Qt::CheckState)value.toInt() == Qt::Checked);
      return true;
    }
  }

  return false;
}

}
}
