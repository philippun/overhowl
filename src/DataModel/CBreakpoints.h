/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CBREAKPOINTS_H
#define CBREAKPOINTS_H

#include "datamodel_global.h"

#include "SBreakpoint.h"
#include "IBreakpoints.h"
#include "IEngineController.h"

#include <QAbstractTableModel>
#include <QMap>

namespace Overhowl {
namespace DataModel {

/**
 * @brief The CBreakpoints class
 */
class DATAMODELSHARED_EXPORT CBreakpoints : public QAbstractTableModel, public IBreakpoints
{
  Q_OBJECT

public:
  CBreakpoints(QObject* parent = nullptr);

  void SetEngineController(IEngineController* engine);

  // IBreakpoints
  void ToggleBreakpoint(const QString& sFilename, int iLineNumber) override;
  QVector<SBreakpoint> GetBreakpoints(void) override;
  bool IsBreakpoint(const QString& sFilename, int iLineNumber) override;

  // QAbstractTableModel
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QModelIndex parent(const QModelIndex &child) const override;
  //QModelIndex index(int row, int column, const QModelIndex &parent) const override;
  Qt::ItemFlags flags(const QModelIndex &index) const override;
  QVariant data(const QModelIndex &index, int role) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  bool setData(const QModelIndex &index, const QVariant &value, int role) override;


private:
  enum eColumns
  {
    ACTIVE,
    LINE,
    FILE,
    COLUMNS
  };

  QVector<SBreakpoint> m_liBreakpoints;
  IEngineController* m_engine = nullptr;
};

}
}

#endif // CBREAKPOINTS_H
