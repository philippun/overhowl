/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CITEMFOLDER_H
#define CITEMFOLDER_H

#include "CSessionItem.h"

#include <QString>

namespace Overhowl {
namespace DataModel {

/**
 * @brief The CItemFolder class
 */
class CItemFolder : public CSessionItem
{
public:
  CItemFolder(QString sName, CSessionItem* parent);
  ~CItemFolder() = default;

  QString IconName() override { return "folder"; }
};

}
}

#endif // CITEMFOLDER_H
