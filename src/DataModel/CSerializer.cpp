/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CSerializer.h"
#include "CSessionItem.h"
#include "CItemScript.h"
#include "CItemFactory.h"
#include "CController.h"

#include <QFile>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

namespace Overhowl {
namespace DataModel {

const QString CSerializer::c_sOverhowlVersion = "0.1";
const QString CSerializer::c_sOverhowlExtension = ".owl";

CSerializer::CSerializer(CMainModel* model)
  : m_model(model)
{

}

QString CSerializer::CreateSessionPath(QString sFolder)
{
  QString sName = QDir(sFolder).dirName();
  QString sFilename = sFolder + QDir::separator() + sName;
  if (sFilename.endsWith(c_sOverhowlExtension) == false)
  {
    sFilename += c_sOverhowlExtension;
  }

  return sFilename;
}

void CSerializer::NewFile(QString sFolder, bool bSave)
{
  if (bSave)
  {
    StoreFile();
  }

  // create directory object
  QDir dir(sFolder);

  if (dir.exists())
  {
    // create session name, which will be the last folder part
    QString sName = dir.dirName();
    m_sFilename = CreateSessionPath(sFolder);

    // reset the model and add a new session with the given name
    m_model->Clear();
    CSessionItem* session = CItemFactory::CreateItem(sName, CSessionItem::Session, m_model->GetRootItem());
    session->m_sPath = dir.absolutePath();
    m_model->AddSession(session);

    // save the newly created session to the file
    StoreFile();

    emit FileLoaded();
    emit SessionLoaded(sName, true);
  }
}

void CSerializer::LoadFile(QString sFilename, bool bSave)
{
  if (bSave)
  {
    StoreFile();
  }

  // assign session filename
  if (sFilename.startsWith("~/"))
  {
    sFilename.replace(0, 1, QDir::homePath());
  }
  m_sFilename = sFilename;

  m_model->Clear();
  CSessionItem::ResetID();
  m_model->DataSaved();

  // read file and parse the json data
  QFile file(m_sFilename);
  QString content;
  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    content = file.readAll();
  }

  QJsonParseError err;
  QJsonDocument jDoc = QJsonDocument::fromJson(content.toUtf8(), &err);

  // parse session data
  QString session = LoadDocument(jDoc, QFileInfo(sFilename).absolutePath());

  emit FileLoaded();
  emit SessionLoaded(session, true);
}

void CSerializer::StoreFile()
{
  m_model->Lock();

  // get the json object from model data
  QJsonObject jObject = StoreDocument();
  QJsonDocument jDoc(jObject);

  // save it to file
  QFile file(m_sFilename);
  file.open(QIODevice::WriteOnly | QIODevice::Text);
  if (file.isOpen())
  {
    file.write(jDoc.toJson());
    m_model->DataSaved();
  }

  m_model->Unlock();
}

void CSerializer::CloseFile(bool bSave)
{
  // close the file, save model data if necessary
  if (bSave)
  {
    StoreFile();
  }

  m_model->Clear();
  m_sFilename.clear();

  emit SessionLoaded("", false);
}

QString CSerializer::LoadDocument(const QJsonDocument& jDoc, QString sPath)
{
  m_model->Lock();

  QString sSession;
  // load data from json document into model
  QJsonObject jObj = jDoc.object();

  if (jObj.value(c_tagOverhowl).isUndefined() == false && jObj.value(c_tagSession).isUndefined() == false)
  {
    QJsonObject jSession = jObj.value(c_tagSession).toObject();
    sSession = jSession.value(c_tagName).toString();

    QString sVersion = jObj.value(c_tagOverhowl).toString();

    if (sSession.isEmpty() == false && sVersion == c_sOverhowlVersion)
    {
      // session item (there can only be one)
      CSessionItem* session = CItemFactory::CreateItem(sSession, CSessionItem::Session, m_model->GetRootItem(), 0);
      session->m_sPath = sPath;
      m_model->AddSession(session);

      // read project data from json project array
      QJsonArray jProjects = jSession.value(c_tagProjects).toArray();
      for (int i = 0; i < jProjects.size(); ++i)
      {
        LoadProject(jProjects[i].toObject(), session);
      }
    }
  }

  m_model->Unlock();

  return sSession;
}

void CSerializer::LoadProject(const QJsonObject& jProj, CSessionItem* session)
{
  QString sName = jProj.value(c_tagProject).toString();
  if (sName.isEmpty() == false)
  {
    // create project item with values from json
    CSessionItem* project = CItemFactory::CreateItem(sName, CSessionItem::Project, session, jProj.value(c_tagID).toInt());
    m_model->AddItem(project, session);

    // add scripts from project
    QJsonArray jScripts = jProj.value(c_tagScripts).toArray();
    for (int i = 0; i < jScripts.size(); ++i)
    {
      QJsonObject jScript = jScripts[i].toObject();
      CItemScript* script = dynamic_cast<CItemScript*> (CItemFactory::CreateItem(jScript.value(c_tagScript).toString(), CSessionItem::Script, project, jScript.value(c_tagID).toInt()));

      // load results for the script
      QJsonArray jResults = jScript.value(c_tagResults).toArray();
      for(int j = 0; j < jResults.size(); ++j)
      {
        QJsonArray jResult = jResults[j].toArray();
      }

      m_model->AddItem(script, project);
    }
  }
}

QJsonObject CSerializer::StoreDocument()
{
  QJsonObject jObj;

  // overhowl version metadata
  jObj.insert(c_tagOverhowl, QJsonValue::fromVariant(c_sOverhowlVersion));

  if (CSessionItem* session = m_model->GetSessionItem())
  {
    // session data
    QJsonObject jSession;
    jSession.insert(c_tagName, session->m_sName);

    // create json project data
    QJsonArray jProjects = StoreProjects(session);
    jSession.insert(c_tagProjects, jProjects);

    jObj.insert(c_tagSession, jSession);
  }

  return jObj;
}

QJsonArray CSerializer::StoreProjects(CSessionItem* session)
{
  QJsonArray jProjects;

  if (session != nullptr)
  {
    // create projects as json
    for (int i = 0; i < session->ChildCount(); ++i)
    {
      CSessionItem* child = session->Child(i);
      if (child->Type() == CSessionItem::Project)
      {
        QJsonObject jProj;
        jProj.insert(c_tagID, QJsonValue::fromVariant(child->ID()));
        jProj.insert(c_tagProject, QJsonValue::fromVariant(child->m_sName));

        QJsonArray jScripts = StoreScripts(child);
        jProj.insert(c_tagScripts, jScripts);

        jProjects.push_back(jProj);
      }
    }
  }

  return jProjects;
}

QJsonArray CSerializer::StoreScripts(CSessionItem* project)
{
  QJsonArray jScripts;

  for (int i = 0; i < project->ChildCount(); ++i)
  {
    CSessionItem* child = project->Child(i);
    if (child->Type() == CSessionItem::Script)
    {
      QJsonObject jScript;
      jScript.insert(c_tagID, QJsonValue::fromVariant(child->ID()));
      jScript.insert(c_tagScript, QJsonValue::fromVariant(child->m_sName));

      //@todo include paths etc

      jScripts.push_back(jScript);
    }
  }

  return jScripts;
}

}
}
