/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CSessionItem.h"
#include "CMainModel.h"

#include <QVariant>
#include <QDir>
#include <QMessageBox>

namespace Overhowl {
namespace DataModel {

int CSessionItem::m_iCurrentID = -1;

CSessionItem::CSessionItem(QString sName, EType eType, CSessionItem* parent)
  : m_sName(sName)
  , m_eType(eType)
  , m_parentItem(parent)
{
  if (eType == EType::Project || eType == EType::Folder)
  {
    // create the project/folder subfolder
    QDir().mkdir(m_parentItem->GetPath() + QDir::separator() + m_sName);
  }
}

CSessionItem::~CSessionItem()
{
  for (CSessionItem* item : m_liChildItems)
  {
    if (item != nullptr)
    {
      delete item;
    }
  }
}

QString CSessionItem::GetPath()
{
  QString sPath;

  if (m_eType == EType::Session)
  {
    sPath = m_sPath;
  }
  else
  {
    if (m_parentItem != nullptr)
    {
      sPath = m_parentItem->GetPath() + QDir::separator();
    }
    sPath += m_sName;
  }

  return sPath;
}

QString CSessionItem::GetName()
{
  return m_sName;
}

QString CSessionItem::GetFullName()
{
  return GetPath();
}

void CSessionItem::AddChild(CSessionItem* child)
{
  m_liChildItems.append(child);
}

void CSessionItem::InsertChild(CSessionItem* child, int iPos)
{
  m_liChildItems.insert(iPos, child);
}

void CSessionItem::RemoveChild(int iID)
{
  for (auto it = m_liChildItems.begin(); it != m_liChildItems.end();)
  {
    if ((*it)->ID() == iID)
    {
      it = m_liChildItems.erase(it);
    }
    else
    {
      ++it;
    }
  }
}

void CSessionItem::RemoveChildAt(int iPos)
{
  m_liChildItems.removeAt(iPos);
}

void CSessionItem::MoveChild(int source, int destination)
{
  if (source < destination)
  {
    --destination;
  }

  auto item = m_liChildItems.at(source);
  m_liChildItems.removeAt(source);
  m_liChildItems.insert(destination, item);
}

void CSessionItem::SetID(int iID)
{
  if (iID > m_iCurrentID)
  {
    m_iID = iID;
    m_iCurrentID = iID;
  }
  else
  {
    m_iID = ++m_iCurrentID;
    CMainModel::Instance().SetModified();
  }
}

bool CSessionItem::HasChild(QString sName)
{
  for (auto child : m_liChildItems)
  {
    if (child->GetName() == sName)
    {
      return true;
    }
  }

  return false;
}

CSessionItem* CSessionItem::Child(int row)
{
  return m_liChildItems[row];
}

int CSessionItem::ChildCount() const
{
  return m_liChildItems.size();
}

int CSessionItem::ColumnCount() const
{
  return 1;
}

QVariant CSessionItem::data(int column) const
{
  if (column == 0)
  {
    return m_sName;
  }

  return QVariant();
}

int CSessionItem::Row() const
{
  if (m_parentItem)
  {
    return m_parentItem->m_liChildItems.indexOf(const_cast<CSessionItem*>(this));
  }

  return 0;
}

CSessionItem* CSessionItem::ParentItem()
{
  return m_parentItem;
}

void CSessionItem::SetParentItem(CSessionItem* parent)
{
  if (parent && m_parentItem)
  {
    // set new parent and move the folder/script to its new location
    QString sOldPath = GetFullName();
    m_parentItem = parent;
    QString sNewPath = GetFullName();

    bool bBackup = false;
    if (QDir(sNewPath).exists())
    {
      QDir().rename(sNewPath, sNewPath + ".bak");
      bBackup = true;
    }
    if (QFile(sNewPath).exists())
    {
      QFile(sNewPath).rename(sNewPath + ".bak");
      bBackup = true;
    }

    if (QFile(sOldPath).exists())
    {
      QFile(sOldPath).rename(sNewPath);
    }
    else if(QDir(sOldPath).exists())
    {
      QDir().rename(sOldPath, sNewPath);
    }

    if (bBackup)
    {
      QMessageBox::warning(nullptr, "backup created", "file or folder not belonging to this session has been moved to a backup location");
    }
  }
}

}
}
