/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CMainModel.h"
#include "CItemFactory.h"
#include "CItemScript.h"

#include <QIcon>
#include <QMutexLocker>
#include <QMimeData>

namespace Overhowl {
namespace DataModel {

CMainModel* CMainModel::m_instance = nullptr;

CMainModel::CMainModel(QObject* parent)
  : QAbstractItemModel(parent)
  , m_lock(QMutex::Recursive)
{
  CSessionItem* topLevel = CItemFactory::CreateTopLevelItem();

  beginResetModel();
  m_rootItem = topLevel;
  endResetModel();

  // data changed slots to set modified flag
  connect(this, &CMainModel::dataChanged, this, &CMainModel::DataChangedSlot);
  connect(this, &CMainModel::rowsInserted, this, &CMainModel::DataInserted);
  connect(this, &CMainModel::rowsMoved, this, &CMainModel::DataMoved);
  connect(this, &CMainModel::rowsRemoved, this, &CMainModel::DataRemoved);
  connect(this, &CMainModel::columnsInserted, this, &CMainModel::DataInserted);
  connect(this, &CMainModel::columnsMoved, this, &CMainModel::DataMoved);
  connect(this, &CMainModel::columnsRemoved, this, &CMainModel::DataRemoved);
  connect(this, &CMainModel::SignalModified, this, &CMainModel::ModifiedSlot);
}

CMainModel& CMainModel::Instance(QObject* parent)
{
  if (m_instance == nullptr)
  {
    m_instance = new CMainModel(parent);
  }

  return *m_instance;
}

bool CMainModel::canDropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) const
{
  QMutexLocker lock(&m_lock);
  bool bCanDrop = false;

  if (action == Qt::MoveAction && data->hasFormat(m_sMimeType))
  {
    auto parentItem = FindItem(static_cast<int>(parent.internalId()));
    auto item = FindItem(data->data(m_sMimeType).toInt());

    if (parentItem && item && (parentItem->HasChild(item->GetName()) == false || item->ParentItem() == parentItem))
    {
      if (parentItem->Type() == CSessionItem::Session)
      {
        bCanDrop = (item->Type() == CSessionItem::Project || item->Type() == CSessionItem::Script);
      }
      else if (parentItem->Type() == CSessionItem::Project)
      {
        bCanDrop = (item->Type() == CSessionItem::Script);
      }
    }
  }

  return bCanDrop;
}

int CMainModel::columnCount(const QModelIndex& /*parent*/) const
{
  return 1;
}

int CMainModel::rowCount(const QModelIndex &parent) const
{
  QMutexLocker lock(&m_lock);

  CSessionItem* parentItem = m_rootItem;

  if(parent.isValid())
  {
    parentItem = FindItem(static_cast<int>(parent.internalId()));
  }

  if (parentItem == nullptr)
  {
    return 0;
  }

  return parentItem->ChildCount();
}

bool CMainModel::moveRows(const QModelIndex& sourceParent, int sourceRow, int count, const QModelIndex& destinationParent, int destinationChild)
{
  QMutexLocker lock(&m_lock);

  if (count > 1)
  {
    // we only want to move one item at once
    return false;
  }

  auto sourceParentItem = FindItem(static_cast<int>(sourceParent.internalId()));
  auto destinationParentItem = FindItem(static_cast<int>(destinationParent.internalId()));
  auto sourceItem = sourceParentItem->Child(sourceRow);

  if (sourceItem == sourceParentItem || sourceItem == destinationParentItem)
  {
    return false;
  }

  if (sourceParentItem == destinationParentItem)
  {
    beginMoveRows(sourceParent, sourceRow, sourceRow, destinationParent, destinationChild);

    sourceParentItem->MoveChild(sourceRow, destinationChild);

    endMoveRows();
  }
  else
  {
    beginMoveRows(sourceParent, sourceRow, sourceRow, destinationParent, destinationChild);

    sourceItem->SetParentItem(destinationParentItem);
    sourceParentItem->RemoveChild(sourceItem->ID());
    destinationParentItem->InsertChild(sourceItem, destinationChild);

    endMoveRows();
  }

  return true;
}

QModelIndex CMainModel::parent(const QModelIndex& index) const
{
  if(!index.isValid())
  {
    return QModelIndex();
  }

  CSessionItem* childItem = FindItem(static_cast<int>(index.internalId()));
  if (childItem)
  {
    CSessionItem* parentItem = childItem->ParentItem();

    if (parentItem == m_rootItem)
    {
      return QModelIndex();
    }

    return createIndex(parentItem->Row(), 0, parentItem->ID());
  }

  return QModelIndex();
}

bool CMainModel::removeColumns(int column, int count, const QModelIndex& parent)
{
  return QAbstractItemModel::removeColumns(column, count, parent);
}

bool CMainModel::removeRows(int row, int count, const QModelIndex& parent)
{
  QMutexLocker locker(&m_lock);

  if (auto parentItem = FindItem(static_cast<int>(parent.internalId())))
  {
    int i = 0;
//    beginRemoveRows(parent, row, row + count - 1);
//    parentItem->RemoveChildAt(row);
//    endRemoveRows();
//    return true;
  }

  return QAbstractItemModel::removeRows(row, count, parent);
}

QModelIndex CMainModel::index(int row, int column, const QModelIndex& parent) const
{
  QMutexLocker locker(&m_lock);

  if (!hasIndex(row, column, parent))
  {
    return QModelIndex();
  }

  // get parent item (root or internally stored)
  CSessionItem* parentItem = m_rootItem;
  if(parent.isValid())
  {
    parentItem = FindItem(static_cast<int>(parent.internalId()));
  }

  // create index for child item if available
  if (parentItem)
  {
    if (CSessionItem* childItem = parentItem->Child(row))
    {
      return createIndex(row, column, childItem->ID());
    }
  }

  return QModelIndex();
}

Qt::ItemFlags CMainModel::flags(const QModelIndex& index) const
{
  if (!index.isValid())
  {
    return Qt::NoItemFlags;
  }

  return QAbstractItemModel::flags(index) | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
}

QVariant CMainModel::data(const QModelIndex& index, int role) const
{
  QMutexLocker locker(&m_lock);

  if (!index.isValid())
  {
    return QModelIndex();
  }

  if (CSessionItem* item = FindItem(static_cast<int>(index.internalId())))
  {
    // get data to be displayed
    if (role == Qt::DisplayRole)
    {
      return item->data(0);
    }

    // get decoration item
    if (role == Qt::DecorationRole)
    {
      return QIcon::fromTheme(item->IconName());
    }
  }

  return QVariant();
}

bool CMainModel::dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent)
{
  QMutexLocker lock(&m_lock);

  if (action == Qt::MoveAction && data->hasFormat(m_sMimeType))
  {
    auto item = FindItem(data->data(m_sMimeType).toInt());
    auto parentItem = item->ParentItem();

    if (item && parentItem)
    {
      auto sourceParentIndex = createIndex(parentItem->Row(), 0, parentItem->ID());

      moveRow(sourceParentIndex, item->Row(), parent, row);
      //@todo returning true atm not possible! (removeRows is called for some reason...)
      //return true;
    }
  }

  return false;
}

QVariant CMainModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  // table header (empty string)
  if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
  {
    if (m_rootItem != nullptr)
    {
      return m_rootItem->data(section);
    }
  }

  return QVariant();
}

QMimeData* CMainModel::mimeData(const QModelIndexList& indexes) const
{
  if (indexes.count() == 1)
  {
    QMimeData* mime = new QMimeData();
    QByteArray data = QByteArray::number(indexes[0].internalId());
    mime->setData(m_sMimeType, data);
    return mime;
  }

  return nullptr;
}

QStringList CMainModel::mimeTypes() const
{
  return {m_sMimeType};
}

Qt::DropActions CMainModel::supportedDropActions() const
{
  return Qt::MoveAction;
}

void CMainModel::AddItem(QString sName, CSessionItem::EType eType, int iParentID)
{
  QMutexLocker lock(&m_lock);

  auto parent = FindItem(iParentID);
  AddItem(CItemFactory::CreateItem(sName, eType, parent), parent);
}

bool CMainModel::Modified()
{
  return m_bModified;
}

void CMainModel::Clear()
{
  QMutexLocker locker(&m_lock);
  beginResetModel();

  // delete item tree recursively
  if (m_rootItem != nullptr && m_rootItem->ChildCount() > 0)
  {
    while (m_rootItem->ChildCount() > 0)
    {
      if (CSessionItem* child = m_rootItem->Child(0))
      {
        delete child;
      }
      m_rootItem->m_liChildItems.pop_front();
    }
  }

  endResetModel();
}

void CMainModel::DataSaved()
{
  m_bModified = false;
}

void CMainModel::SetModified()
{
  emit SignalModified();
}

CSessionItem::EType CMainModel::TypeForItem(int iID)
{
  QMutexLocker lock(&m_lock);

  if (CSessionItem* item = FindItem(iID))
  {
    return item->Type();
  }

  return CSessionItem::None;
}

QString CMainModel::FilenameForItem(int iID)
{
  QMutexLocker lock(&m_lock);

  if (CItemScript* item = dynamic_cast<CItemScript*>(FindItem(iID)))
  {
    return item->GetFullName();
  }

  return "";
}

bool CMainModel::DuplicateCheck(int iID, QString sName)
{
  QMutexLocker lock(&m_lock);

  if (auto item = FindItem(iID))
  {
    return item->HasChild(sName);
  }

  return false;
}

void CMainModel::DataChangedSlot(const QModelIndex& /*topLeft*/, const QModelIndex& /*bottomRight*/, const QVector<int>& /*roles*/)
{
  m_bModified = true;
}

void CMainModel::DataInserted(const QModelIndex& /*parent*/, int /*first*/, int /*last*/)
{
  m_bModified = true;
}

void CMainModel::DataMoved(const QModelIndex& /*parent*/, int /*start*/, int /*end*/, const QModelIndex& /*destination*/, int /*where*/)
{
  m_bModified = true;
}

void CMainModel::DataRemoved(const QModelIndex& /*parent*/, int /*first*/, int /*last*/)
{
  m_bModified = true;
}

void CMainModel::AddSession(CSessionItem* session)
{
  // only allow one session item
  if (m_rootItem != nullptr && m_rootItem->ChildCount() == 0)
  {
    beginResetModel();
    m_rootItem->AddChild(session);
    endResetModel();
  }
}

void CMainModel::AddItem(CSessionItem* item, CSessionItem* parent)
{
  // only allow one session item
  if (item->Type() == CSessionItem::Session || item->Type() == CSessionItem::None)
  {
    return;
  }

  if (parent)
  {
    beginInsertRows(createIndex(parent->Row(), 0, parent->ID()), parent->ChildCount(), parent->ChildCount() + 1);

    // add session item to its designated parent
    parent->AddChild(item);

    endInsertRows();
  }
}

void CMainModel::Lock()
{
  m_lock.lock();
}

void CMainModel::Unlock()
{
  m_lock.unlock();
}

CSessionItem* CMainModel::FindItem(int iID) const
{
  bool bDelete = false;
  return FindItem(m_rootItem, iID, bDelete);
}

CSessionItem* CMainModel::FindItem(CSessionItem* item, int iID, bool& bRemove) const
{
  CSessionItem* found = nullptr;

  if (item->ID() == iID)
  {
    found = item;
  }

  for (int i = 0; found == nullptr && i < item->ChildCount(); ++i)
  {
    found = FindItem(item->Child(i), iID, bRemove);

    if (found && bRemove)
    {
      item->RemoveChildAt(i);
      bRemove = false;
    }
  }

  return found;
}

CSessionItem* CMainModel::RemoveItem(int iID) const
{
  bool bRemove = true;
  return FindItem(m_rootItem, iID, bRemove);
}

void CMainModel::ModifiedSlot()
{
  m_bModified = true;
}

CSessionItem*CMainModel::GetSessionItem()
{
  if (m_rootItem->ChildCount() > 0)
  {
    return m_rootItem->Child(0);
  }

  return nullptr;
}

}
}
