/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CSERIALIZER_H
#define CSERIALIZER_H

#include "datamodel_global.h"

#include "CMainModel.h"

#include <QString>

namespace Overhowl {
namespace DataModel {

class CSessionItem;

/**
 * @brief The CSerializer class
 */
class DATAMODELSHARED_EXPORT CSerializer : public QObject
{
  Q_OBJECT

public:
  CSerializer(CMainModel* model);

  static QString CreateSessionPath(QString sPath);

public slots:
  void NewFile(QString sFolder, bool bSave);
  void LoadFile(QString sFilename, bool bSave);
  void StoreFile();
  void CloseFile(bool bSave);


signals:
  void FileLoaded();
  void SessionLoaded(QString sSession, bool bLoaded);


private:
  QString LoadDocument(const QJsonDocument& jDoc, QString sPath);
  void LoadProject(const QJsonObject& jProj, CSessionItem* session);

  QJsonObject StoreDocument();
  QJsonArray StoreProjects(CSessionItem* session);
  QJsonArray StoreScripts(CSessionItem* project);

private:
  CMainModel* m_model;
  QString m_sFilename;

  static const QString c_sOverhowlVersion;
  static const QString c_sOverhowlExtension;

  const QString c_tagOverhowl = "overhowl";
  const QString c_tagSession = "session";
  const QString c_tagProjects = "projects";
  const QString c_tagProject = "project";
  const QString c_tagScripts = "scripts";
  const QString c_tagScript = "script";
  const QString c_tagResults = "results";
  const QString c_tagName = "name";
  const QString c_tagID = "id";
};

}
}

#endif // CSERIALIZER_H
