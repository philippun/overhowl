/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CPLUGINS_H
#define CPLUGINS_H

#include "datamodel_global.h"

#include "IPlugin.h"
#include "IPlugins.h"

#include <QList>
#include <QAbstractTableModel>
#include <QThread>

namespace Overhowl {
namespace DataModel {


class DATAMODELSHARED_EXPORT CPlugins : public QAbstractTableModel, public IPlugins
{
  Q_OBJECT

public:
  CPlugins();
  virtual ~CPlugins() override;

  void LoadPlugins(void);
  void SessionClosed(void);
  void SessionOpened(QString sName);

  // QAbstractTableModel
  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;
  Qt::ItemFlags flags(const QModelIndex &index) const override;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  bool setData(const QModelIndex &index, const QVariant &value, int role) override;

  // IPlugins
  const QList<SPlugin>& GetPlugins() const override;


signals:
  void SessionClosedSignal();
  void SessionOpenedSignal(QString sName);
  void ActivatePlugin(SPlugin plugin);


private:
  QList<SPlugin> m_liPlugins; //@todo lock the plugins list, i.e. when scriptcontroller loads all the script classes
  QThread* m_thread = nullptr;

private:
  enum eColumns
  {
    NAME,
    ACTIVE,
    COLUMNS
  };
};

}
}

#endif // CPLUGINS_H
