/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CSESSIONITEM_H
#define CSESSIONITEM_H

#include <QList>
class QVariant;

namespace Overhowl {
namespace DataModel {

class CSessionItem
{
  friend class CMainModel;
  friend class CSerializer;
  friend class CItemFactory;

public:
  enum EType
  {
    None = 0,
    Session,
    Script,
    Project,
    Folder,
  };

protected:
  CSessionItem(QString sName, EType eType, CSessionItem* parent = nullptr);

public:
  virtual ~CSessionItem();

  CSessionItem* Child(int row);
  int ChildCount() const;
  int ColumnCount() const;
  virtual QVariant data(int column) const;
  int Row() const;
  CSessionItem* ParentItem();
  void SetParentItem(CSessionItem* parent);
  virtual QString IconName() { return ""; }

  int ID() { return m_iID; }
  EType Type() { return m_eType; }
  QString GetPath();
  QString GetName();
  virtual QString GetFullName();

protected:
  void AddChild(CSessionItem* child);
  void InsertChild(CSessionItem* child, int iPos);
  void RemoveChild(int iID);
  void RemoveChildAt(int iPos);
  void MoveChild(int source, int destination);
  void SetID(int iID = -1);
  bool HasChild(QString sName);

private:
  static void ResetID() { m_iCurrentID = -1; }


protected:
  EType m_eType;
  QString m_sName;
  QString m_sPath;

  CSessionItem* m_parentItem = nullptr;

private:
  int m_iID = -1;
  static int m_iCurrentID;

  QList<CSessionItem*> m_liChildItems;
};

}
}

#endif // CSESSIONITEM_H
