/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CPlugins.h"

#include <QPluginLoader>
#include <QApplication>
#include <QDir>
#include <QStandardPaths>

namespace Overhowl {
namespace DataModel {

CPlugins::CPlugins()
{
  m_thread = new QThread();
  m_thread->start();
}

CPlugins::~CPlugins()
{
  m_thread->quit();

  while(m_thread->isFinished() == false)
  {
    QThread::msleep(100);
  }
}

void CPlugins::LoadPlugins()
{
#ifndef NDEBUG
  QDir pluginsDir(QApplication::applicationDirPath()
#ifdef __linux__
    + QDir::separator() + "plugins"
#endif
  );
#else
  QDir pluginsDir(QStandardPaths::standardLocations(QStandardPaths::DataLocation)[0]);
#endif
  for (QString sPath : pluginsDir.entryList())
  {
    QPluginLoader loader(pluginsDir.absoluteFilePath(sPath));
    QString err = loader.errorString();
    QObject* obj = loader.instance();
    err = loader.errorString();

    bool bExists = false;

    if (IPlugin* plugin = dynamic_cast<IPlugin*>(obj))
    {
      for (auto existingPlugin : m_liPlugins)
      {
        if (existingPlugin.plugin->PluginName() == plugin->PluginName())
        {
          bExists = true;
          break;
        }
      }

      if (bExists == false)
      {
        m_liPlugins.push_back(SPlugin{ plugin, true });
        plugin->moveToThread(m_thread);

        connect(this, &CPlugins::SessionClosedSignal, plugin, &IPlugin::SessionClosed);
        connect(this, &CPlugins::SessionOpenedSignal, plugin, &IPlugin::SessionOpened);
      }
    }
  }
}

void CPlugins::SessionClosed()
{
  emit SessionClosedSignal();
}

void CPlugins::SessionOpened(QString sName)
{
  emit SessionOpenedSignal(sName);
}

int CPlugins::rowCount(const QModelIndex& /*parent*/) const
{
  return m_liPlugins.size();
}

int CPlugins::columnCount(const QModelIndex& /*parent*/) const
{
  return COLUMNS;
}

Qt::ItemFlags CPlugins::flags(const QModelIndex& index) const
{
  if (!index.isValid())
  {
    return Qt::NoItemFlags;
  }

  if (index.column() == ACTIVE)
  {
    return QAbstractTableModel::flags(index) | Qt::ItemIsUserCheckable;
  }

  return QAbstractTableModel::flags(index);
}

QVariant CPlugins::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
  {
    return QVariant();
  }

  if (role == Qt::DisplayRole)
  {
    switch(index.column())
    {
      case NAME:
        return m_liPlugins[index.row()].plugin->PluginName();
    }
  }
  else if (role == Qt::CheckStateRole)
  {
    if (index.column() == ACTIVE)
    {
      return (m_liPlugins[index.row()].bLoad ? Qt::Checked : Qt::Unchecked);
    }
  }
  else if (role == Qt::TextAlignmentRole)
  {
    return Qt::AlignCenter;
  }

  return QVariant();
}

QVariant CPlugins::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
  {
    switch (section)
    {
    case NAME:
      return "  Name  ";

    case ACTIVE:
      return "  Active  ";
    }
  }

  return QVariant();
}

bool CPlugins::setData(const QModelIndex& index, const QVariant& value, int role)
{
  if (!index.isValid())
  {
    return false;
  }

  if (role == Qt::CheckStateRole)
  {
    if (index.column() == ACTIVE)
    {
      m_liPlugins[index.row()].bLoad = (static_cast<Qt::CheckState>(value.toInt()) == Qt::Checked);
      emit ActivatePlugin(m_liPlugins[index.row()]);
      return true;
    }
  }

  return false;
}

const QList<SPlugin>& CPlugins::GetPlugins() const
{
  return m_liPlugins;
}

}
}
