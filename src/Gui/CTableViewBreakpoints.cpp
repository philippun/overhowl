/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CTableViewBreakpoints.h"

#include <QHeaderView>

namespace Overhowl {

CTableViewBreakpoints::CTableViewBreakpoints(QWidget* parent)
  : QTableView(parent)
{
  setSelectionBehavior(QAbstractItemView::SelectRows);
  setSelectionMode(QAbstractItemView::SingleSelection);
  horizontalHeader()->setStretchLastSection(true);
  verticalHeader()->hide();
}

void CTableViewBreakpoints::setModel(QAbstractItemModel* model)
{
  QTableView::setModel(model);

  resizeColumnToContents(0);
  resizeColumnToContents(1);
}

}
