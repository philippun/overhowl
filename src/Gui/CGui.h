/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CGUI_H
#define CGUI_H

#include "gui_global.h"

#include "IGui.h"
#include "IEngineController.h"
#include <QObject>


namespace Overhowl {


/**
 * @brief The CGui class
 */
class GUISHARED_EXPORT CGui : public QObject, public IGui
{
  Q_OBJECT

public:
  CGui(IEngineController* engine);

  void WriteOutput(QString sText) override;
  void WriteLog(QString sText) override;
  void WriteTrace(QString sText) override;
  void ShowStatusBarMessage(QString sText) override;


signals:
  void SignalWriteOutput(QString sText);
  void SignalWriteLog(QString sText);
  void SignalWriteTrace(QString sText);
  void SignalShowStatusBarMessage(QString sText);


private:
  IEngineController* m_engine = nullptr;
};

}

#endif // CGUI_H
