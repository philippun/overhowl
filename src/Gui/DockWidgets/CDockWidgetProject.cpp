/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CDockWidgetProject.h"
#include "ui_DockWidgetProject.h"
#include "CMainModel.h"


namespace Overhowl {

CDockWidgetProject::CDockWidgetProject(QWidget *parent) :
  QDockWidget(parent),
  ui(new Ui::CDockWidgetProject)
{
  ui->setupUi(this);
  m_treeViewProject = ui->treeViewProject;

  //@todo add toolbar
}

CDockWidgetProject::~CDockWidgetProject()
{
  delete ui;
}

void CDockWidgetProject::SetModel(DataModel::CMainModel* model)
{
  m_treeViewProject->SetModel(model);
}

void CDockWidgetProject::AssignActions(QAction* addScript, QAction* addProject)
{
  m_treeViewProject->AssignActions(addScript, addProject);

  //@todo add actions to toolbar (to be implemented)
}

}

