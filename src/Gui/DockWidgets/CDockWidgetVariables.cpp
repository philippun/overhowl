/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CDockWidgetVariables.h"
#include "ui_DockWidgetVariables.h"

#include <QKeyEvent>
#include <QClipboard>


namespace Overhowl {

CDockWidgetVariables::CDockWidgetVariables(QWidget *parent) :
  QDockWidget(parent),
  ui(new Ui::CDockWidgetVariables)
{
  ui->setupUi(this);
  m_Variables = ui->treeWidgetVariables;

  m_Variables->setHeaderLabels({"Name", "Value"});
  m_Variables->setStyleSheet("QTreeWidget::item:!selected { border-bottom: 1px solid black; }"
                             "QTreeWidget::item:first:!selected { border-right: 1px solid black; }"
                             "QTreeWidget::item:selected {}");

  connect(m_Variables, &QTreeWidget::itemDoubleClicked, this, &CDockWidgetVariables::ItemDoubleClicked);
}

CDockWidgetVariables::~CDockWidgetVariables()
{
  delete ui;
}

void CDockWidgetVariables::SetLocals(QList<ScriptEngine::SVariable> liLocals)
{
  m_Variables->clear();

  AddSubItems(nullptr, liLocals);
}

void CDockWidgetVariables::ItemDoubleClicked(QTreeWidgetItem* item, int column)
{
  QApplication::clipboard()->setText(item->text(column));
}

void CDockWidgetVariables::AddSubItems(QTreeWidgetItem* item, QList<ScriptEngine::SVariable> liLocals)
{
  for (ScriptEngine::SVariable var : liLocals)
  {
    QTreeWidgetItem* child = nullptr;
    if (item == nullptr)
    {
      child = new QTreeWidgetItem(m_Variables);
    }
    else
    {
      child = new QTreeWidgetItem();
    }
    child->setFlags(child->flags() & (~Qt::ItemIsEditable));

    child->setText(0, var.Name);

    if (var.Children.size() > 0)
    {
      AddSubItems(child, var.Children);
    }
    else
    {
      if (var.Value.isEmpty())
      {
        child->setText(1, "[not initialized]");
      }
      else
      {
        if (var.Type == "string")
        {
          child->setText(1, "\"" + var.Value + "\"");
        }
        else
        {
          child->setText(1, var.Value);
        }
      }
    }

    if (item != nullptr)
    {
      item->addChild(child);
    }
  }
}

}
