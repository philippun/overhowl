/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CDOCKWIDGETPROJECT_H
#define CDOCKWIDGETPROJECT_H

#include "CTreeViewProject.h"

#include <QDockWidget>
#include <QTreeView>

// forward declarations
namespace Ui {
class CDockWidgetProject;
}


namespace Overhowl {

// forward declarations
class CController;
namespace DataModel {
  class CMainModel;
}

/**
 * @brief The CDockWidgetProject class
 */
class CDockWidgetProject : public QDockWidget
{
  Q_OBJECT
  friend class CMainWindow;

public:
  explicit CDockWidgetProject(QWidget *parent);
  ~CDockWidgetProject();

  void SetModel(DataModel::CMainModel* model);
  void AssignActions(QAction* addScript, QAction* addProject);

private:
  Ui::CDockWidgetProject *ui;
  CTreeViewProject* m_treeViewProject = nullptr;
};

}

#endif // CDOCKWIDGETPROJECT_H
