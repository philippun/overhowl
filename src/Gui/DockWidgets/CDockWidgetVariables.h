/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CDOCKWIDGETVARIABLES_H
#define CDOCKWIDGETVARIABLES_H

#include "SVariable.h"

#include <QDockWidget>
#include <QTreeWidget>

namespace Ui {
class CDockWidgetVariables;
}

namespace Overhowl {

class CDockWidgetVariables : public QDockWidget
{
  Q_OBJECT

public:
  explicit CDockWidgetVariables(QWidget *parent);
  ~CDockWidgetVariables();

  void SetLocals(QList<ScriptEngine::SVariable> liLocals);


public slots:
  void ItemDoubleClicked(QTreeWidgetItem* item, int column);


private:
  void AddSubItems(QTreeWidgetItem* item, QList<ScriptEngine::SVariable> liLocals);


private:
  Ui::CDockWidgetVariables *ui;
  QTreeWidget* m_Variables = nullptr;
};

}

#endif // CDOCKWIDGETVARIABLES_H
