/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CTREEVIEWPROJECT_H
#define CTREEVIEWPROJECT_H

#include "CTreeViewProject.h"
#include "CSessionItem.h"
#include "CItemScript.h"

#include <QTreeWidget>

// forward declarations
namespace Overhowl {
class CMainWindow;
namespace DataModel {
  class CMainModel;
}
}


using namespace Overhowl::DataModel;

namespace Overhowl {

/**
 * @brief The CTreeViewProject class
 */
class CTreeViewProject : public QTreeView
{
  friend class CMainWindow;
  Q_OBJECT

public:
  CTreeViewProject(QWidget* parent = nullptr);
  virtual ~CTreeViewProject() = default;

  void AssignActions(QAction* addScript, QAction* addProject);
  void SetModel(DataModel::CMainModel* model);
  int GetSelectedItem();
  int GetItemWithScript(QString sScript);


private:
  void AddItem(CSessionItem::EType eType);


protected:
  void dropEvent(QDropEvent* event) override;


public slots:
  void AddScript();
  void AddProject();
  void doubleClickedSlot(const QModelIndex& index);
  void selectionChanged(const QItemSelection& selected, const QItemSelection& deselected) override;


signals:
  void OpenScript(QString sName, int iID, bool bSession = true);
  void SelectionChangedType(int eType);
  void SelectionChangedItem(int iID);


private:
  QAction* actionAddScript = nullptr;
  QAction* actionAddProject = nullptr;

  CMainModel* m_MainModel;
};

}

#endif // CTREEVIEWPROJECT_H
