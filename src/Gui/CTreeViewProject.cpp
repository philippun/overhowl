/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CTreeViewProject.h"
#include "CItemFactory.h"
#include "CItemScript.h"
#include "CMainModel.h"

#include <QHeaderView>
#include <QAction>
#include <QDropEvent>
#include <QInputDialog>
#include <QMessageBox>

using namespace Overhowl::DataModel;

namespace Overhowl {

CTreeViewProject::CTreeViewProject(QWidget* parent)
  : QTreeView(parent)
{
  setSelectionBehavior(QAbstractItemView::SelectRows);
  setSelectionMode(QAbstractItemView::SingleSelection);
  //setItemsExpandable(false);
  //setRootIsDecorated(false);
  //header()->hide();

  //setDefaultDropAction(Qt::CopyAction);
  setAcceptDrops(true);
  setDragEnabled(true);
  setDropIndicatorShown(true);
  //setDragDropOverwriteMode(false);
  setDragDropMode(DragDrop);
}

void CTreeViewProject::AssignActions(QAction* addScript, QAction* addProject)
{
  // save actions, we have to enable/disable them depending on what item is selected in the treeview
  actionAddScript = addScript;
  actionAddProject = addProject;

  // connect signals
  connect(actionAddScript, &QAction::triggered, this, &CTreeViewProject::AddScript);
  connect(actionAddProject, &QAction::triggered, this, &CTreeViewProject::AddProject);

  connect(this, &CTreeViewProject::doubleClicked, this, &CTreeViewProject::doubleClickedSlot);
}

void CTreeViewProject::SetModel(DataModel::CMainModel* model)
{
  QTreeView::setModel(model);
  m_MainModel = model;

  expandAll();
}

int CTreeViewProject::GetSelectedItem()
{
  QModelIndexList liSelected = selectedIndexes();

  if (liSelected.size() > 0)
  {
    return static_cast<int>(liSelected.first().internalId());
  }

  return -1;
}

int CTreeViewProject::GetItemWithScript(QString sScript)
{
  return -1;
}

void CTreeViewProject::AddItem(CSessionItem::EType eType)
{
  // get selected item and check if it is allowed to have children
  int iSelectedID = static_cast<int>(currentIndex().internalId());
  auto eSelectedType = static_cast<CSessionItem::EType>(m_MainModel->TypeForItem(iSelectedID));

  if (eSelectedType == static_cast<int>(CSessionItem::EType::Session) ||
      eSelectedType == static_cast<int>(CSessionItem::EType::Folder) ||
      eSelectedType == static_cast<int>(CSessionItem::EType::Project))
  {
    // create type specific dialog string
    QString sType;
    if (eType == CSessionItem::EType::Folder)
    {
      sType = "Folder";
    }
    else if (eType == CSessionItem::EType::Script)
    {
      sType = "Script";
    }
    if (eType == CSessionItem::EType::Project)
    {
      sType = "Project";
    }

    // open a input dialog to request name for the new item
    bool bOk = true;
    bool bDuplicate = true;
    QString sText;
    while (bOk && bDuplicate)
    {
      sText = QInputDialog::getText(this, "Enter a " + sType + " name",
                                    sType + " name:", QLineEdit::Normal,
                                    sText, &bOk);

      // check for duplicate @todo duplicate check in model
      bDuplicate = m_MainModel->DuplicateCheck(iSelectedID, sText);
      if (bDuplicate)
      {
        QMessageBox::warning(this, "Error", "Item with name \"" + sText + "\" does already exist.", QMessageBox::Ok);
      }
    }

    // add item if input is valid
    if (bOk && !sText.isEmpty())
    {
      m_MainModel->AddItem(sText, eType, iSelectedID);
      setExpanded(selectedIndexes()[0], true);
    }
  }
}

void CTreeViewProject::dropEvent(QDropEvent* event)
{
  return QTreeView::dropEvent(event);
}

void CTreeViewProject::AddScript()
{
  AddItem(CSessionItem::EType::Script);
}

void CTreeViewProject::AddProject()
{
  AddItem(CSessionItem::EType::Project);
}

void CTreeViewProject::doubleClickedSlot(const QModelIndex& index)
{
  int iID = static_cast<int>(index.internalId());
  CSessionItem::EType eType = m_MainModel->TypeForItem(iID);

  // open script/manual
  if (eType == CSessionItem::Script)
  {
    // open script file
    emit OpenScript(m_MainModel->FilenameForItem(iID), iID);
  }
}

void CTreeViewProject::selectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
  QTreeView::selectionChanged(selected, deselected);

  if (selected.size() > 0)
  {
    // get selected session item
    int id = static_cast<int>(selected.indexes().first().internalId());
    CSessionItem::EType eType = static_cast<CSessionItem::EType>(m_MainModel->TypeForItem(id));
    // notify about new type selected
    emit SelectionChangedType(eType);
    // notify about new script item (if it is one)
    emit SelectionChangedItem(eType == CSessionItem::Script ? id : -1);
  }
}

}
