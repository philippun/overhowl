/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CGui.h"

namespace Overhowl {

CGui::CGui(IEngineController* engine)
  : m_engine(engine)
{

}

void CGui::WriteOutput(QString sText)
{
  emit SignalWriteOutput(sText);
}

void CGui::WriteLog(QString sText)
{
  emit SignalWriteLog(sText);
}

void CGui::WriteTrace(QString sText)
{
  emit SignalWriteTrace(sText);
}

void CGui::ShowStatusBarMessage(QString sText)
{
  emit SignalShowStatusBarMessage(sText);
}

}
