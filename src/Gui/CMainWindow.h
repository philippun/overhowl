/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "gui_global.h"

#include "CTreeViewProject.h"
#include "IEngineController.h"
#include "IPlugin.h"
#include "SVariable.h"
#include "DockWidgets/CDockWidgetProject.h"

#include <QMainWindow>
#include <QAction>

// forward declarations
namespace Ui {
class CMainWindow;
}
class QTextEdit;

namespace Overhowl {
namespace DataModel {
  class CMainModel;
  class CBreakpoints;
  class CPlugins;
}
class CDockWidgetBreakpoints;
class CDockWidgetVariables;
class CDockWidgetLog;
class CTabWidgetFiles;
class CTableViewResults;
class CController;
class CGuiStateMachine;
class CRecentSessionStorage;

/**
 * @brief The CMainWindow class
 */
class GUISHARED_EXPORT CMainWindow : public QMainWindow
{
  friend class CController;
  friend class CGuiStateMachine;
  Q_OBJECT

public:
  explicit CMainWindow();
  ~CMainWindow() override;

  void SetupWindow(IEngineController& scriptEngine, DataModel::CMainModel& mainModel, DataModel::CBreakpoints& breakpointsModel, DataModel::CPlugins& pluginsModel);
  void UnsetWindow();
  void AddDockWidgets(QList<QPair<EPosition, QDockWidget*>> docks);
  void RemoveDockWidgets(QList<QPair<EPosition, QDockWidget*>> docks);

public slots:
  void SlotWriteOutput(QString sText);
  void SlotWriteLog(QString sText);
  void SlotWriteTrace(QString sText);
  void SlotShowStatusBarMessage(QString sText);
  void SessionLoadedSlot(QString sSession, bool bLoaded);
  void SetWindowTitle(QString title = "");
  void RecentTriggeredSlot(bool bChecked);

protected:
  void closeEvent(QCloseEvent* event) override;

private:
  void ShowStatusMessage(QString sText);
  void AddDockViewActions(QList<QDockWidget*> docks);
  void RemoveDockViewActions(QList<QDockWidget*> docks);
  QTextEdit* Log();
  int CheckSaveSession();

private slots:
  void OpenTriggeredSlot(bool bChecked);
  void OpenSessionTriggeredSlot(bool bChecked);
  void NewSessionTriggeredSlot(bool bChecked);
  void CloseSessionTriggeredSlot(bool bChecked);
  void SaveTriggeredSlot(bool bChecked);
  void UpdateProjectActions(bool bNewScript, bool bNewProject);
  void UpdateDebugActions(bool bRunScript, bool bRunScriptFromProject, bool bRunProject, bool bRunAllProjects,
                          bool bStop, bool bPause, bool bStepOver, bool bStepInto, bool bStepOut);
  void UpdateDebuggingPositionSlot(QString sFile, int iLine);
  void UpdateSessionActionsSlot(bool bNew, bool bOpen, bool bClose);
  void UpdateVariablesViewSlot(bool bEnabled);
  // script engine slots
  void StartAllProjectsTriggeredSlot(bool bChecked);
  void StartPojectTriggeredSlot(bool bChecked);
  void StartScriptFromProjectTriggeredSlot(bool bChecked);
  void StartScriptTriggeredSlot(bool bChecked);
  void StopScriptTriggeredSlot(bool bChecked);
  void PauseScriptTriggeredSlot(bool bChecked);
  void ToggleBreakpointSlot();
  void LocalsUpdatedSlot(QList<ScriptEngine::SVariable> liLocals);
  void OpenPluginsDialog(bool bChecked);


signals:
  void SelectionChangedType(int eType);
  void SelectionChangedItem(int iID);
  void OpenSession(QString sFilename, bool bSave);
  void NewSession(QString sFolder, bool bSave);
  void CloseSession(bool bSave);


private:
  Ui::CMainWindow *ui = nullptr;

  CTreeViewProject* m_project = nullptr;
  CTabWidgetFiles* m_files = nullptr;
  QTextEdit* m_log = nullptr;
  QTextEdit* m_output = nullptr;
  CDockWidgetProject* m_dockProject = nullptr;
  CDockWidgetBreakpoints* m_dockBreakpoints = nullptr;
  CDockWidgetVariables* m_dockVariables = nullptr;
  CDockWidgetLog* m_dockLog = nullptr;
  CRecentSessionStorage* m_recentSessions = nullptr;

  // QActions needed by Controller
  QAction* m_actionPause_Script = nullptr;
  QAction* m_actionResume_Script = nullptr;
  QAction* m_actionStep_Over = nullptr;
  QAction* m_actionStep_Into = nullptr;
  QAction* m_actionStep_Out = nullptr;

  DataModel::CBreakpoints* m_breakpointsModel = nullptr;
  DataModel::CMainModel* m_model = nullptr;
  DataModel::CPlugins* m_pluginsModel = nullptr;

  IEngineController* m_scriptEngine;
};

}

#endif // MAINWINDOW_H
