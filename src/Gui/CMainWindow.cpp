/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CMainWindow.h"
#include "ui_MainWindow.h"
#include "DockWidgets/CDockWidgetProject.h"
#include "DockWidgets/CDockWidgetActiveProjects.h"
#include "DockWidgets/CDockWidgetVariables.h"
#include "DockWidgets/CDockWidgetLog.h"
#include "DockWidgets/CDockWidgetTrace.h"
#include "DockWidgets/CDockWidgetOutput.h"
#include "DockWidgets/CDockWidgetBreakpoints.h"
#include "CController.h"
#include "TextEditor/CTabFile.h"
#include "TextEditor/CTabWidgetFiles.h"
#include "CMainModel.h"
#include "CItemScript.h"
#include "CBreakpoints.h"
#include "CRecentSessionStorage.h"
#include "CPluginsDialog.h"
#include "CSerializer.h"

#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QTableView>
#include <QAction>
#include <QScrollBar>
#include <QTextEdit>
#include <QTextStream>
#include <QInputDialog>


namespace Overhowl {

CMainWindow::CMainWindow()
  : /*QMainWindow(parent)
  , */ui(new Ui::CMainWindow)
{
  ui->setupUi(this);

  SetWindowTitle();

  // dock widgets
  // right: results, variables
  m_dockVariables = new CDockWidgetVariables(this);
  addDockWidget(Qt::RightDockWidgetArea, m_dockVariables);
  m_dockVariables->raise();
  // bottom: log, trace, output, breakpoints
  CDockWidgetLog* m_dockLog = new CDockWidgetLog(this);
  addDockWidget(Qt::BottomDockWidgetArea, m_dockLog);
  CDockWidgetTrace* dockTrace = new CDockWidgetTrace(this);
  tabifyDockWidget(m_dockLog, dockTrace);
  CDockWidgetOutput* dockOutput = new CDockWidgetOutput(this);
  tabifyDockWidget(m_dockLog, dockOutput);
  m_dockBreakpoints = new CDockWidgetBreakpoints(this);
  tabifyDockWidget(m_dockLog, m_dockBreakpoints);
  // left: project
  m_dockProject = new CDockWidgetProject(this);
  addDockWidget(Qt::LeftDockWidgetArea, m_dockProject);
  CDockWidgetActiveProjects* dockActiveProjects = new CDockWidgetActiveProjects(this);
  tabifyDockWidget(m_dockProject, dockActiveProjects);
  dockActiveProjects->setVisible(false);
  m_dockProject->raise();

  // set corners for dock widget areas
  setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
  setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
  setCorner(Qt::TopRightCorner, Qt::RightDockWidgetArea);
  setCorner(Qt::BottomRightCorner, Qt::BottomDockWidgetArea);

  // add dock actions to the view menu
  AddDockViewActions({m_dockProject, dockActiveProjects, m_dockVariables, m_dockLog, dockTrace, dockOutput, m_dockBreakpoints});

  // save relevant tabwidget content as member
  m_project = m_dockProject->m_treeViewProject;
  m_log = m_dockLog->GetLog();
  m_output = dockOutput->GetOutput();

  m_files = ui->tabWidgetFiles;

  // update toolbar button enabled status
  // UpdateProjectToolbar(CSessionItem::None); //@todo

  m_actionPause_Script = ui->actionPause_Script;
  m_actionResume_Script = ui->actionRun_Script;
  m_actionStep_Over = ui->actionStep_Over;
  m_actionStep_Into = ui->actionStep_Into;
  m_actionStep_Out = ui->actionStep_Out;

  m_recentSessions = new CRecentSessionStorage(this, ui->menuRecent_Sessions);

  // connect signals and slots
  connect(ui->actionSave, &QAction::triggered, this, &CMainWindow::SaveTriggeredSlot);
  connect(ui->actionOpen, &QAction::triggered, this, &CMainWindow::OpenTriggeredSlot);
  connect(ui->actionRun_Script, &QAction::triggered, this, &CMainWindow::StartScriptTriggeredSlot);
  connect(ui->actionRun_Script_Project, &QAction::triggered, this, &CMainWindow::StartScriptFromProjectTriggeredSlot);
  connect(ui->actionRun_Project, &QAction::triggered, this, &CMainWindow::StartPojectTriggeredSlot);
  connect(ui->actionRun_All_Projects, &QAction::triggered, this, &CMainWindow::StartAllProjectsTriggeredSlot);
  connect(ui->actionStop_Script, &QAction::triggered, this, &CMainWindow::StopScriptTriggeredSlot);
  connect(ui->actionToggle_Breakpoint, &QAction::triggered, this, &CMainWindow::ToggleBreakpointSlot);
  connect(m_dockProject->m_treeViewProject, &CTreeViewProject::SelectionChangedType, this, &CMainWindow::SelectionChangedType);
  connect(m_dockProject->m_treeViewProject, &CTreeViewProject::SelectionChangedItem, this, &CMainWindow::SelectionChangedItem);
  connect(m_dockProject->m_treeViewProject, &CTreeViewProject::OpenScript, m_files, &CTabWidgetFiles::OpenScript);
  connect(ui->actionOpen_Session, &QAction::triggered, this, &CMainWindow::OpenSessionTriggeredSlot);
  connect(ui->actionNew_Session, &QAction::triggered, this, &CMainWindow::NewSessionTriggeredSlot);
  connect(ui->actionClose_Session, &QAction::triggered, this, &CMainWindow::CloseSessionTriggeredSlot);
  connect(ui->actionPlugins, &QAction::triggered, this, &CMainWindow::OpenPluginsDialog);


  // assign actions to project tab
  m_dockProject->AssignActions(ui->actionNew_Script, ui->actionNew_Project);

  show();
}

CMainWindow::~CMainWindow()
{
  delete ui;

  if (m_recentSessions != nullptr)
  {
    delete m_recentSessions;
  }
}

void CMainWindow::SetupWindow(IEngineController& scriptEngine, DataModel::CMainModel& mainModel, DataModel::CBreakpoints& breakpointsModel, DataModel::CPlugins& pluginsModel)
{
  m_scriptEngine= &scriptEngine;
  m_model = &mainModel;
  m_breakpointsModel = &breakpointsModel;
  m_pluginsModel = &pluginsModel;
  m_files->SetBreakpoints(m_breakpointsModel);

  m_dockProject->SetModel(m_model);
  m_dockBreakpoints->SetModel(*m_breakpointsModel);
}

void CMainWindow::UnsetWindow()
{
  m_dockProject->SetModel(nullptr);
}

void CMainWindow::AddDockWidgets(QList<QPair<EPosition, QDockWidget*>> docks)
{
  for (auto dock : docks)
  {
    AddDockViewActions({dock.second});
    switch (dock.first)
    {
      case EPosition::Left:
        tabifyDockWidget(m_dockProject, dock.second);
        break;

      case EPosition::Right:
        tabifyDockWidget(m_dockVariables, dock.second);
        break;

      case EPosition::Bottom:
        tabifyDockWidget(m_dockLog, dock.second);
        break;
    }
    QMetaObject::invokeMethod(dock.second, "setVisible", Q_ARG(bool, true));
  }
}

void CMainWindow::RemoveDockWidgets(QList<QPair<EPosition, QDockWidget*> > docks)
{
  for (auto dock : docks)
  {
    RemoveDockViewActions({dock.second});
    removeDockWidget(dock.second);
  }
}

void CMainWindow::SlotWriteOutput(QString sText)
{
  m_output->setText(m_output->toPlainText() + sText + "\n");

  m_output->verticalScrollBar()->setValue(m_output->verticalScrollBar()->maximum());
}

void CMainWindow::SlotWriteLog(QString sText)
{
  m_log->setText(m_log->toPlainText() + sText + "\n");

  m_log->verticalScrollBar()->setValue(m_log->verticalScrollBar()->maximum());
}

void CMainWindow::SlotWriteTrace(QString sText)
{
  //@todo trace
}

void CMainWindow::SlotShowStatusBarMessage(QString sText)
{
  ui->statusBar->showMessage(sText, 1000);
}

void CMainWindow::SessionLoadedSlot(QString sSession, bool bLoaded)
{
  SetWindowTitle(sSession);
}

void CMainWindow::SetWindowTitle(QString title)
{
  setWindowTitle("Overhowl" + (title.isEmpty() ? "" : " - " + title));
}

void CMainWindow::closeEvent(QCloseEvent* event)
{
  switch (CheckSaveSession())
  {
    case QMessageBox::Yes:
      emit CloseSession(true);
      QMainWindow::closeEvent(event);
      break;
    case QMessageBox::No:
      QMainWindow::closeEvent(event);
      break;
    default:
      event->ignore();
      break;
  }
}

void CMainWindow::AddDockViewActions(QList<QDockWidget*> docks)
{
  for (QDockWidget* dock : docks)
  {
    if (dock->titleBarWidget() == nullptr)
    {
      dock->setTitleBarWidget(new QWidget());
    }
    ui->menuView->addAction(dock->toggleViewAction());
  }
}

void CMainWindow::RemoveDockViewActions(QList<QDockWidget*> docks)
{
  for (QDockWidget* dock : docks)
  {
    ui->menuView->removeAction(dock->toggleViewAction());
  }
}

void CMainWindow::OpenTriggeredSlot(bool /*bChecked*/)
{
  QString sFilename = QFileDialog::getOpenFileName(this,
      tr("Open File"), QDir::currentPath(), tr("Script Files (*.js *.script)"));

  if (sFilename.isEmpty() == false)
  {
    m_files->OpenScript(sFilename, false);
  }
}

void CMainWindow::OpenSessionTriggeredSlot(bool /*bChecked*/)
{
  QString sFilename;

  // close old session first
  int iResult = CheckSaveSession();

  if (iResult != QMessageBox::Cancel)
  {
    sFilename = QFileDialog::getOpenFileName(this, tr("Open Session"), QDir::currentPath(), tr("Overhowl Files (*.owl *.json)"));

    if (sFilename.isEmpty() == false)
    {
      emit OpenSession(sFilename, iResult == QMessageBox::Yes);
      m_recentSessions->AddSession(sFilename);
    }
  }
}

void CMainWindow::NewSessionTriggeredSlot(bool /*bChecked*/)
{
  bool bSave = CheckSaveSession() == QMessageBox::Yes;

  QString sFolder = QFileDialog::getExistingDirectory(this,
      tr("Open Folder"), QDir::currentPath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

  if (sFolder.isEmpty() == false && QDir(sFolder).exists())
  {
    emit NewSession(sFolder, bSave);
    m_recentSessions->AddSession(CSerializer::CreateSessionPath(sFolder));
  }
}

void CMainWindow::CloseSessionTriggeredSlot(bool /*bChecked*/)
{
  int iResult = CheckSaveSession();

  if (iResult != QMessageBox::Cancel)
  {
    emit CloseSession(iResult == QMessageBox::Yes);
  }
}

void CMainWindow::SaveTriggeredSlot(bool /*bChecked*/)
{
  m_files->SaveCurrentTab();
}

void CMainWindow::UpdateProjectActions(bool bNewScript, bool bNewProject)
{
  ui->actionNew_Script->setEnabled(bNewScript);
  ui->actionNew_Project->setEnabled(bNewProject);
}

void CMainWindow::UpdateDebugActions(bool bRunScript, bool bRunScriptFromProject, bool bRunProject, bool bRunAllProjects, bool bStop, bool bPause, bool bStepOver, bool bStepInto, bool bStepOut)
{
  ui->actionRun_Script->setEnabled(bRunScript);
  ui->actionRun_Script_Project->setEnabled(bRunScriptFromProject);
  ui->actionRun_Project->setEnabled(bRunProject);
  ui->actionRun_All_Projects->setEnabled(bRunAllProjects);

  ui->actionStop_Script->setEnabled(bStop);
  ui->actionPause_Script->setEnabled(bPause);
  ui->actionStep_Over->setEnabled(bStepOver);
  ui->actionStep_Into->setEnabled(bStepInto);
  ui->actionStep_Out->setEnabled(bStepOut);
}

void CMainWindow::UpdateDebuggingPositionSlot(QString sFile, int iLine)
{
  m_files->SetCurrentStatement(sFile, iLine);
}

void CMainWindow::UpdateSessionActionsSlot(bool bNew, bool bOpen, bool bClose)
{
  ui->actionNew_Session->setEnabled(bNew);
  ui->actionOpen_Session->setEnabled(bOpen);
  ui->actionClose_Session->setEnabled(bClose);
}

void CMainWindow::UpdateVariablesViewSlot(bool bEnabled)
{
  m_dockVariables->setEnabled(bEnabled);
}

void CMainWindow::RecentTriggeredSlot(bool /*bChecked*/)
{
  QAction* snd = qobject_cast<QAction*>(sender());
  emit OpenSession(snd->data().toString(), CheckSaveSession());
  m_recentSessions->AddSession(snd->data().toString());
}

void CMainWindow::StartAllProjectsTriggeredSlot(bool)
{
  //@todo start all projects
}

void CMainWindow::StartPojectTriggeredSlot(bool /*bChecked*/)
{
  //@todo start one project
}

void CMainWindow::StartScriptFromProjectTriggeredSlot(bool /*bChecked*/)
{
  if (m_scriptEngine->IsRunning())
  {
    m_scriptEngine->Run();
  }
  else
  {
    int iID = m_project->GetSelectedItem();

    QString sFilename = m_model->FilenameForItem(iID);
    m_files->OpenScript(sFilename, iID);
    // @todo there could be some problems when a new script is opened before the execution starts

    QFile oInput(sFilename);

    if (oInput.open(QIODevice::ReadOnly))
    {
      QString sFileContent;
      QTextStream oStream(&oInput);

      while(!oStream.atEnd())
      {
        sFileContent += oStream.readLine() + "\n";
      }

      m_scriptEngine->RunScript(sFileContent, sFilename, iID);
    }
  }
}

void CMainWindow::StartScriptTriggeredSlot(bool)
{
  if (m_scriptEngine->IsRunning())
  {
    m_scriptEngine->Run();
  }
  else
  {
    if (CTabFile* oFile = m_files->GetCurrentTab())
    {
      m_scriptEngine->RunScript(oFile->GetScriptText(), oFile->GetScriptFullName(), oFile->GetScriptID());
    }
  }
}

void CMainWindow::StopScriptTriggeredSlot(bool)
{
  if (m_scriptEngine->IsRunning())
  {
    m_scriptEngine->Stop();
  }
}

void CMainWindow::PauseScriptTriggeredSlot(bool)
{
  m_scriptEngine->Pause();
}

void CMainWindow::ToggleBreakpointSlot()
{
  // get current tab and toggle its current filename, line as breakpoint
  if (CTabFile* fileTab = m_files->GetCurrentTab())
  {
    m_breakpointsModel->ToggleBreakpoint(fileTab->GetScriptFullName(), fileTab->m_editor->GetCurrentLine() + 1);

    // update the breakpoint area
    fileTab->update();
  }
}

void CMainWindow::LocalsUpdatedSlot(QList<ScriptEngine::SVariable> liLocals)
{
  m_dockVariables->SetLocals(liLocals);
}

void CMainWindow::OpenPluginsDialog(bool /*bChecked*/)
{
  CPluginsDialog dial(this);
  dial.SetModel(m_pluginsModel);
  dial.exec();
}

QTextEdit* CMainWindow::Log()
{
  return m_log;
}

int CMainWindow::CheckSaveSession()
{
  if(m_model->Modified())
  {
    return QMessageBox::Yes;
    //return QMessageBox::question(this, "Save", "Do you want to save the session?", QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, QMessageBox::No);
  }

  return QMessageBox::No;
}

}
