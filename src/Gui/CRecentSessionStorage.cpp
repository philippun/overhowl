/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CController.h"
#include "CRecentSessionStorage.h"

#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QAction>

namespace Overhowl {

CRecentSessionStorage::CRecentSessionStorage(CMainWindow* parent, QMenu* menu)
  : m_parent(parent)
  , m_menu(menu)
  , m_filename(/*CController::sAppData + QDir::separator() +*/ "sessions")
{
  QFile file(m_filename);
  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    QTextStream stream(&file);
    while (stream.atEnd() == false)
    {
      AddSession(stream.readLine(), false);
    }

    SaveSessions();
  }
}

void CRecentSessionStorage::AddSession(QString path, bool bSave)
{
  QList<SessionData> backup = m_sessions;

  m_sessions.clear();

  for (SessionData data : backup)
  {
    if (data.path != path)
    {
      m_sessions.push_back(data);
    }
  }

  if (QDir().exists(path))
  {
    if (bSave)
    {
      m_sessions.push_front(SessionData({path.split(QDir::separator()).last(), path}));
    }
    else
    {
      m_sessions.push_back(SessionData({path.split(QDir::separator()).last(), path}));
    }
  }

  m_menu->clear();

  for (SessionData data : m_sessions)
  {
    QAction* action = new QAction();
    action->setText(data.name);
    action->setData(data.path);
    connect(action, &QAction::triggered, m_parent, &CMainWindow::RecentTriggeredSlot);
    m_menu->addAction(action);
  }

  if (bSave)
  {
    SaveSessions();
  }
}

void CRecentSessionStorage::SaveSessions()
{
  QFile file(m_filename);
  if (file.open(QIODevice::WriteOnly))
  {
    QTextStream stream(&file);
    for (SessionData data : m_sessions)
    {
      stream << data.path << "\n";
    }
  }
}

}
