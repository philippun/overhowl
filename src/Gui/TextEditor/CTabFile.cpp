/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CTabFile.h"
#include "CTabWidgetFiles.h"
#include "CSyntaxHighlighter.h"
#include "CBreakpoints.h"

#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QMessageBox>

using namespace Overhowl::DataModel;

namespace Overhowl {

CTabFile::CTabFile(QString sFilename, int iID, CBreakpoints* breakPoints, CTabWidgetFiles* parent)
  : QWidget(parent)
  , m_sFullFileName(sFilename)
  , m_iID(iID)
  , m_tabParent(parent)
{
  // create elements and set size
  QHBoxLayout* oLayout = new QHBoxLayout(this);
  setLayout(oLayout);
  oLayout->addWidget(m_editor);
  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  // create text editor and highlighter
  m_editor = new CTextEditor(breakPoints, m_sFullFileName, this);
  layout()->addWidget(m_editor);
  m_highlighter = new CSyntaxHighlighter(m_editor->document());

  // fill text edit and add file watcher
  LoadFile();

  // connect cursor position changed //@todo
  //connect(m_editor, &QPlainTextEdit::cursorPositionChanged, this, &CTabFile::SelectionChanged);

  // connect script document modified signal
  connect(m_editor->document(), &QTextDocument::modificationChanged, this, &CTabFile::UpdateTabLabel);
  // set document modified state to false
  m_editor->document()->setModified(false);
}

CTabFile::~CTabFile()
{
}

QString CTabFile::GetTabName(bool bChanged)
{
  if (m_sFullFileName.isEmpty())
  {
    return QString("unnamed*");
  }

  return m_sFullFileName.split("/").last() + (bChanged ? "*" : "");
}

bool CTabFile::LoadFile()
{
  if (!m_sFullFileName.isEmpty())
  {
    QFile oInput(m_sFullFileName);
    m_editor->clear();

    if (oInput.open(QIODevice::ReadOnly))
    {
      QTextStream oStream(&oInput);

      while(!oStream.atEnd())
      {
        m_editor->appendPlainText(oStream.readLine());
      }

      if (m_fileWatcher != nullptr)
      {
        delete m_fileWatcher;
      }
      m_fileWatcher = new QFileSystemWatcher();
      if (m_fileWatcher != nullptr)
      {
        m_fileWatcher->addPath(m_sFullFileName);
        connect(m_fileWatcher, &QFileSystemWatcher::fileChanged, this, &CTabFile::FileChangedSlot);
      }
      m_editor->document()->setModified(false);

      return true;
    }
  }

  return false;
}

void CTabFile::Save()
{
  if (m_sFullFileName.isEmpty())
  {
    m_sFullFileName = QFileDialog::getSaveFileName(this,
        tr("Save File"), QDir::currentPath(), tr("Script Files (*.script *.js)"));

    // @todo own file dialog which adds extension

    //QStringList liNameSplit = m_sFullFileName.split(".");
    //if (liNameSplit.size() > 1)
    //{
    //  if(liNameSplit.last() != "script"
    //     && liNameSplit.last() != "js")
    //  {
    //    m_sFullFileName += ".script";
    //  }
    //}
  }

  // save file
  QFile oOutput(m_sFullFileName);

  if (oOutput.open(QIODevice::WriteOnly))
  {
    QTextStream oStream(&oOutput);

    oStream << m_editor->toPlainText();

    // set document to not modified
    m_editor->document()->setModified(false);
  }
}

bool CTabFile::SaveBeforeClose()
{
  // if document was modified, a dialog will ask if it has to be saved
  if (m_editor->document()->isModified())
  {
    bool bCloseTab = false;

    QMessageBox oDialog;
    oDialog.setText("Do you want to save the file?");
    oDialog.setInformativeText("The document about to be closed has been modified.");
    oDialog.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    oDialog.setDefaultButton(QMessageBox::Save);

    switch(oDialog.exec())
    {
      case QMessageBox::Save:
      Save();
      case QMessageBox::Discard:
      bCloseTab = true;
      break;
    }

    return bCloseTab;
  }

  return true;
}

QString CTabFile::GetScriptText()
{
  return m_editor->toPlainText();
}

QString CTabFile::GetScriptName()
{
  return m_sFullFileName.split("/").last();
}

QString CTabFile::GetScriptFullName()
{
  return m_sFullFileName;
}

int CTabFile::GetScriptID()
{
  return m_iID;
}

void CTabFile::SetCurrentStatement(int iLine)
{
  m_editor->SetCurrentStatement(iLine);
}

void CTabFile::SetSession(bool bSession)
{
  m_bSession = bSession;
}

void CTabFile::SelectionChanged()
{
  QTextCharFormat format;
  format.setForeground(Qt::darkCyan);

  QTextCursor oCursor = m_editor->textCursor();
  oCursor.select(QTextCursor::WordUnderCursor);

  QTextEdit::ExtraSelection selection;
  selection.cursor = oCursor;
  selection.format.setBackground(Qt::lightGray);

  QList<QTextEdit::ExtraSelection> extraSelections;
  extraSelections.append(selection);

  m_editor->setExtraSelections(extraSelections);
}

void CTabFile::UpdateTabLabel(bool changed)
{
  m_tabParent->UpdateTabText(GetTabName(changed), qobject_cast<QWidget*>(this));
}

void CTabFile::FileChangedSlot(const QString& path)
{
  if (path == m_sFullFileName)
  {
    //@todo ask if file should be reloaded, flag as modified if not reloaded
    LoadFile();
  }
}

}
