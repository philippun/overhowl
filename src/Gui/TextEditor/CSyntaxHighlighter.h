/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CSYNTAXHIGHLIGHTER_H
#define CSYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>

namespace Overhowl {

class CSyntaxHighlighter : public QSyntaxHighlighter
{
  Q_OBJECT

public:
  CSyntaxHighlighter(QTextDocument* document);

  void highlightBlock(const QString &sText) override;
  static void SetupHighlightingRules();

private:
  void HighlightKeywords(const QString &sText);
  void HighlightBlocks(const QString &sText);
  void HighlightSingleBlock(const QString &sText);
  void FindBlockStart(const QString &sText);
  bool FindBlockEnd(const QString &sText);


private:
  struct HighlightingRule
  {
      QRegExp pattern;
      QTextCharFormat format;
  };

  enum class EBlockType
  {
    Unassigned = -1,
    None,
    SingleLineComment,
    MultiLineComment,
    SingleQuoteString,
    DoubleQuoteString
  };

  int m_iBlockPosition = 0;
  int m_iBlockLength = 0;
  EBlockType m_eBlockType = EBlockType::None;

  static QVector<QString> m_liBlockIdentifier;
  static QVector<HighlightingRule> m_liHighlightingRules;

  static QRegExp m_regexCommentStartExpression;
  static QRegExp m_regexCommentEndExpression;

  static QTextCharFormat m_formatKeyword;
  static QTextCharFormat m_formatClass;
  static QTextCharFormat m_formatFunction;
  static QTextCharFormat m_formatString;
  static QTextCharFormat m_formatComment;
  static QTextCharFormat m_formatHighlighted;

  static const QString c_sSingleQuote;
  static const QString c_sDoubleQuote;
  static const QString c_sSingleLineComment;
  static const QString c_sMultiLineComment;
  static const QString c_sMultiLineCommentEnd;

  static const QStringList c_liBlockIdentifier;
  static const QStringList c_liBlockIdentifierEnd;

  static bool m_bInitialized;
};

}

#endif // CSYNTAXHIGHLIGHTER_H
