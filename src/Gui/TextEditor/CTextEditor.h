/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CTEXTEDITOR_H
#define CTEXTEDITOR_H

#include <QPlainTextEdit>


namespace Overhowl {
namespace DataModel {
  class CBreakpoints;
}

/**
 * @brief The CTextEditor class
 */
class CTextEditor : public QPlainTextEdit
{
  Q_OBJECT

public:
  CTextEditor(DataModel::CBreakpoints* breakPoints, const QString& sFilename,  QWidget* parent = nullptr);

  void lineNumberAreaPaintEvent(QPaintEvent *event);
  int lineNumberAreaWidth();
  int GetCurrentLine();
  void SetCurrentStatement(int iLine);

protected:
  void resizeEvent(QResizeEvent* e) override;
  void keyPressEvent(QKeyEvent* e) override;

private slots:
  void updateLineNumberAreaWidth(int newBlockCount);
  void highlightCurrentLine();
  void updateLineNumberArea(const QRect &, int);


private:
  QWidget* m_lineNumberArea;
  DataModel::CBreakpoints* m_breakPoints;
  QString m_sFilename;
  int m_iStatement = 0;
};

}

#endif // CTEXTEDITOR_H
