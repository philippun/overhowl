/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CTABFILE_H
#define CTABFILE_H

#include <QWidget>
#include <QLayout>
#include <QFileSystemWatcher>

#include "CTextEditor.h"


namespace Overhowl {

// forward declarations
class CSyntaxHighlighter;
namespace DataModel {
 class CBreakpoints;
}
class CTabWidgetFiles;

/**
 * @brief The CTabFile class
 */
class CTabFile : public QWidget
{
  friend class CMainWindow;
  Q_OBJECT

public:
  explicit CTabFile(QString sFilename, int iID, DataModel::CBreakpoints* breakPoints, CTabWidgetFiles *parent);
  ~CTabFile();

  QString GetTabName(bool bChanged = false);

  bool LoadFile();
  void Save();
  bool SaveBeforeClose();
  QString GetScriptText();
  QString GetScriptName();
  QString GetScriptFullName();
  int GetScriptID();
  void SetCurrentStatement(int iLine);
  void SetSession(bool bSession);
  bool GetSession()
  {
    return m_bSession;
  }


public slots:
  void SelectionChanged();
  void UpdateTabLabel(bool changed);
  void FileChangedSlot(const QString& path);


private:
  QString m_sFullFileName;
  int m_iID = -1;
  CTextEditor* m_editor = nullptr;
  QFileSystemWatcher* m_fileWatcher = nullptr;
  CTabWidgetFiles* m_tabParent = nullptr;
  CSyntaxHighlighter* m_highlighter = nullptr;
  bool m_bSession = false;
};

}

#endif // CTABFILE_H
