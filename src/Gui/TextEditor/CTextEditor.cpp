/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CTextEditor.h"
#include "CLineNumberArea.h"
#include "CBreakpoints.h"

#include <QPainter>
#include <QTextBlock>
#include <QTabWidget>

using namespace Overhowl::DataModel;

namespace Overhowl {

CTextEditor::CTextEditor(CBreakpoints* breakPoints, const QString& sFilename, QWidget* parent)
  : QPlainTextEdit(parent)
  , m_breakPoints(breakPoints)
  , m_sFilename(sFilename)
{
  //@asserts

  m_lineNumberArea = new CLineNumberArea(this);

  connect(this, &CTextEditor::blockCountChanged, this, &CTextEditor::updateLineNumberAreaWidth);
  connect(this, &CTextEditor::updateRequest, this, &CTextEditor::updateLineNumberArea);
  connect(this, &CTextEditor::cursorPositionChanged, this, &CTextEditor::highlightCurrentLine);

  // show tabs and spaces
  QTextOption option = document()->defaultTextOption();
  option.setFlags(option.flags() | QTextOption::ShowTabsAndSpaces);
  document()->setDefaultTextOption(option);

  // monospace font
  QFont font("Source Code Pro");
  font.setStyleHint(QFont::TypeWriter);
  font.setPointSize(10);
  document()->setDefaultFont(font);

  updateLineNumberAreaWidth(0);
  highlightCurrentLine();
}

int CTextEditor::lineNumberAreaWidth()
{
  int digits = 1;
  int max = qMax(1, blockCount());
  while (max >= 10)
  {
    max /= 10;
    ++digits;
  }

  int space = 3 + fontMetrics().width(QLatin1Char('9')) * digits + 10;

  return space;
}

int CTextEditor::GetCurrentLine()
{
  return textCursor().blockNumber();
}

void CTextEditor::SetCurrentStatement(int iLine)
{
  m_iStatement = iLine;
  m_lineNumberArea->update();
}

void CTextEditor::updateLineNumberAreaWidth(int /* newBlockCount */)
{
  setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void CTextEditor::updateLineNumberArea(const QRect &rect, int dy)
{
  if (dy)
  {
    m_lineNumberArea->scroll(0, dy);
  }
  else
  {
    m_lineNumberArea->update(0, rect.y(), m_lineNumberArea->width(), rect.height());
  }

  if (rect.contains(viewport()->rect()))
  {
    updateLineNumberAreaWidth(0);
  }
}

void CTextEditor::resizeEvent(QResizeEvent *e)
{
  QPlainTextEdit::resizeEvent(e);

  QRect cr = contentsRect();
  m_lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void CTextEditor::keyPressEvent(QKeyEvent* e)
{
  if (e->key() == Qt::Key_Tab && e->modifiers() == Qt::NoModifier)
  {
    insertPlainText("  ");
    return;
  }

  QPlainTextEdit::keyPressEvent(e);
}

void CTextEditor::highlightCurrentLine()
{
  QList<QTextEdit::ExtraSelection> extraSelections;

  if (!isReadOnly()) {
    QTextEdit::ExtraSelection selection;

    QColor lineColor = QColor(Qt::yellow).lighter(160);

    selection.format.setBackground(lineColor);
    selection.format.setProperty(QTextFormat::FullWidthSelection, true);
    selection.cursor = textCursor();
    selection.cursor.clearSelection();
    extraSelections.append(selection);
  }

  setExtraSelections(extraSelections);
}

void CTextEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
  QPainter painter(m_lineNumberArea);
  painter.fillRect(event->rect(), Qt::lightGray);
  QTextBlock block = firstVisibleBlock();
  int blockNumber = block.blockNumber() + 1;
  int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
  int bottom = top + (int) blockBoundingRect(block).height();

  while (block.isValid() && top <= event->rect().bottom())
  {
    if (block.isVisible() && bottom >= event->rect().top())
    {
      QString number = QString::number(blockNumber);
      painter.setPen(Qt::black);
      painter.drawText(0, top, m_lineNumberArea->width(), fontMetrics().height(),
                       Qt::AlignRight, number);

      if (m_breakPoints->IsBreakpoint(m_sFilename, blockNumber))
      {
        painter.setBrush(Qt::red);
        painter.drawEllipse(0, top + 4, fontMetrics().height() * 0.55, fontMetrics().height() * 0.55);
      }

      if (blockNumber == m_iStatement)
      {
        painter.setBrush(Qt::yellow);
        painter.drawRect(4, top + fontMetrics().height() * 0.5 - 2, 7, 4);
      }
    }

    block = block.next();
    top = bottom;
    bottom = top + (int) blockBoundingRect(block).height();
    ++blockNumber;
  }
}

}
