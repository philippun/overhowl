/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CTABWIDGETFILES_H
#define CTABWIDGETFILES_H

#include "gui_global.h"

#include <QTabWidget>

namespace Overhowl {

// forward declarations
namespace DataModel {
 class CBreakpoints;
}
class CTabFile;

/**
 * @brief The CTabWidgetFiles class
 */
class GUISHARED_EXPORT CTabWidgetFiles : public QTabWidget
{
  Q_OBJECT

public:
  CTabWidgetFiles(QWidget* pParent = nullptr);

  void SaveCurrentTab();
  CTabFile* GetCurrentTab();
  void UpdateTabText(QString sText, QWidget* oTab);
  void SetBreakpoints(DataModel::CBreakpoints* breakPoints);
  void SetCurrentStatement(QString sFile, int iLine);

public slots:
  void OpenScript(QString sName, int iID, bool bSession = true);
  void CloseTab(int index);
  void SessionLoadedSlot(QString sSession, bool bLoaded);


signals:
  void ScriptOpen(bool bOpen);


private:
  bool BringTabToTopIfExists(QString sFilename, bool bSession = true);
  int AddTab(CTabFile* oTab, bool bSession = true);

private:
  DataModel::CBreakpoints* m_breakPoints = nullptr;

};

}

#endif // CTABWIDGETFILES_H
