/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CTabWidgetFiles.h"
#include "CTabFile.h"
#include "CSyntaxHighlighter.h"
#include "CBreakpoints.h"

#include <QDir>

using namespace Overhowl::DataModel;

namespace Overhowl {

CTabWidgetFiles::CTabWidgetFiles(QWidget* pParent)
  : QTabWidget(pParent)
{
  setTabsClosable(true);
  setMovable(true);

  CSyntaxHighlighter::SetupHighlightingRules();

  connect(this, &QTabWidget::tabCloseRequested, this, &CTabWidgetFiles::CloseTab);
}

int CTabWidgetFiles::AddTab(CTabFile* oTab, bool bSession)
{
  int iTab = addTab(oTab, oTab->GetTabName());
  setCurrentWidget(oTab);
  oTab->SetSession(bSession);

  // notify the gui state machine, that a new script has been opened
  if (count() == 1)
  {
    emit ScriptOpen(true);
  }

  return iTab;
}

void CTabWidgetFiles::SaveCurrentTab()
{
  QWidget* oWidget = currentWidget();
  if (oWidget != nullptr)
  {
    CTabFile* oTab = qobject_cast<CTabFile*>(oWidget);

    oTab->Save();
  }
}

void CTabWidgetFiles::OpenScript(QString sName, int iID, bool bSession)
{
  // add new tab with given file or bring existing tab to the top
  if (!BringTabToTopIfExists(sName, bSession))
  {
    AddTab(new CTabFile(sName, iID, m_breakPoints, this), bSession);
  }
}

CTabFile* CTabWidgetFiles::GetCurrentTab()
{
  return qobject_cast<CTabFile*>(currentWidget());
}

void CTabWidgetFiles::UpdateTabText(QString sText, QWidget *oTab)
{
  setTabText(indexOf(oTab), sText);
}

void CTabWidgetFiles::SetBreakpoints(DataModel::CBreakpoints* breakPoints)
{
  m_breakPoints = breakPoints;
}

void CTabWidgetFiles::SetCurrentStatement(QString sFile, int iLine)
{
  bool bFound = false;

  for (int i = 0; i < count(); ++i)
  {
    CTabFile* oTab = qobject_cast<CTabFile*>(widget(i));
    oTab->SetCurrentStatement(0);

    if (oTab->GetScriptFullName() == sFile)
    {
      setCurrentIndex(i);
      oTab->SetCurrentStatement(iLine);

      bFound = true;
    }
  }

  if (!bFound && !sFile.isEmpty())
  {
    OpenScript(sFile, -1);
    GetCurrentTab()->SetCurrentStatement(iLine);
  }
}

void CTabWidgetFiles::CloseTab(int index)
{
  // get widget from tab at position index and delete it
  if(CTabFile* oTab = qobject_cast<CTabFile*>(widget(index)))
  {
    // check if we have to delete the tab or cancel was pushed in the dialog
    if (oTab->SaveBeforeClose())
    {
      // remove tab and delete widget
      removeTab(index);
      delete oTab;
    }
  }

  // notify gui state machine that no script is open
  if (count() == 0)
  {
    emit ScriptOpen(false);
  }
}

void CTabWidgetFiles::SessionLoadedSlot(QString /*sSession*/, bool bLoaded)
{
  if (bLoaded == false)
  {
    // close all files that are part of the recently closed session
    for (int i = count() - 1; i >= 0; --i)
    {
      CTabFile* oTab = qobject_cast<CTabFile*>(widget(i));
      if (oTab != nullptr && oTab->GetSession())
      {
        removeTab(i);
      }
    }
  }
}

bool CTabWidgetFiles::BringTabToTopIfExists(QString sFilename, bool /*bSession*/)
{
  for (int i = 0; i < count(); ++i)
  {
    CTabFile* oTab = qobject_cast<CTabFile*>(widget(i));

    if (oTab->GetScriptFullName() == sFilename)
    {
      setCurrentIndex(i);
      return true;
    }
  }

  return false;
}

}
