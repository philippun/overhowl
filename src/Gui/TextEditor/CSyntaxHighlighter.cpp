/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CSyntaxHighlighter.h"

#include <algorithm>

namespace Overhowl {

QVector<QString> CSyntaxHighlighter::m_liBlockIdentifier;
QVector<CSyntaxHighlighter::HighlightingRule> CSyntaxHighlighter::m_liHighlightingRules;
QRegExp CSyntaxHighlighter::m_regexCommentStartExpression;
QRegExp CSyntaxHighlighter::m_regexCommentEndExpression;
QTextCharFormat CSyntaxHighlighter::m_formatKeyword;
QTextCharFormat CSyntaxHighlighter::m_formatClass;
QTextCharFormat CSyntaxHighlighter::m_formatFunction;
QTextCharFormat CSyntaxHighlighter::m_formatString;
QTextCharFormat CSyntaxHighlighter::m_formatComment;
QTextCharFormat CSyntaxHighlighter::m_formatHighlighted;


const QStringList CSyntaxHighlighter::c_liBlockIdentifier     = { "none", "//", "/*", "'", "\"" };
const QStringList CSyntaxHighlighter::c_liBlockIdentifierEnd  = { "none", "\n", "*/", "'", "\"" };

bool CSyntaxHighlighter::m_bInitialized = false;

CSyntaxHighlighter::CSyntaxHighlighter(QTextDocument* document)
  : QSyntaxHighlighter(document)
{
}

void CSyntaxHighlighter::SetupHighlightingRules()
{
  if (!m_bInitialized)
  {
    HighlightingRule rule;

    // define keyword format
    m_formatKeyword.setForeground(Qt::darkBlue);
    m_formatKeyword.setFontWeight(QFont::Bold);

    // define keywords
    QStringList keywordPatterns;
    keywordPatterns << "\\bbreak\\b"
                    << "\\bdo\\b"
                    << "\\bin\\b"
                    << "\\btypeof\\b"
                    << "\\bcase\\b"
                    << "\\belse\\b"
                    << "\\binstanceof\\b"
                    << "\\bvar\\b"
                    << "\\bcatch\\b"
                    << "\\bexport\\b"
                    << "\\bnew\\b"
                    << "\\bvoid\\b"
                    << "\\bclass\\b"
                    << "\\bextends\\b"
                    << "\\breturn\\b"
                    << "\\bwhile\\b"
                    << "\\bconst\\b"
                    << "\\bfinally\\b"
                    << "\\bsuper\\b"
                    << "\\bwith\\b"
                    << "\\bcontinue\\b"
                    << "\\bfor\\b"
                    << "\\bswitch\\b"
                    << "\\byield\\b"
                    << "\\bdebugger\\b"
                    << "\\bfunction\\b"
                    << "\\bthis\\b"
                    << "\\bdefault\\b"
                    << "\\bif\\b"
                    << "\\bthrow\\b"
                    << "\\bdelete\\b"
                    << "\\bimport\\b"
                    << "\\btry\\b";

    // add keyword and format to the highlighting rules
    for (const QString &pattern : keywordPatterns)
    {
      rule.pattern = QRegExp(pattern);
      rule.format = m_formatKeyword;
      m_liHighlightingRules.append(rule);
    }

    // block identifier
    m_liBlockIdentifier << "";

    // class format
    m_formatClass.setFontWeight(QFont::Bold);
    m_formatClass.setForeground(Qt::darkMagenta);
    rule.pattern = QRegExp("\\bQ[A-Za-z]+\\b");
    rule.format = m_formatClass;
    m_liHighlightingRules.append(rule);

    // function format
    m_formatFunction.setFontItalic(true);
    m_formatFunction.setForeground(Qt::blue);
    rule.pattern = QRegExp("\\b[A-Za-z0-9_]+(?=\\()");
    rule.format = m_formatFunction;
    m_liHighlightingRules.append(rule);

    // string format
    m_formatString.setForeground(Qt::darkGreen);

    // comment format
    m_formatComment.setForeground(Qt::darkGray);

    // highlighted word format
    m_formatHighlighted.setBackground(Qt::lightGray);

    m_bInitialized = true;
  }
}

void CSyntaxHighlighter::highlightBlock(const QString &sText)
{
  HighlightKeywords(sText);

  HighlightBlocks(sText);
}

void CSyntaxHighlighter::HighlightKeywords(const QString &sText)
{
  for (const HighlightingRule &rule : m_liHighlightingRules)
  {
    QRegExp expression(rule.pattern);
    int index = expression.indexIn(sText);
    while (index >= 0)
    {
        int length = expression.matchedLength();
        setFormat(index, length, rule.format);
        index = expression.indexIn(sText, index + length);
    }
  }
}

void CSyntaxHighlighter::HighlightBlocks(const QString &sText)
{
  m_iBlockPosition = 0;
  m_eBlockType = static_cast<EBlockType>(previousBlockState());
  // ignore single line comment in previous block
  if (m_eBlockType == EBlockType::SingleLineComment)
  {
    m_eBlockType = EBlockType::None;
  }


  while (m_iBlockPosition < sText.size())
  {
    if (m_eBlockType <= EBlockType::None)
    {
      FindBlockStart(sText);
    }
    else
    {
      m_iBlockPosition = 0;
    }

    if (m_eBlockType > EBlockType::None)
    {
      bool bFound = FindBlockEnd(sText);
      HighlightSingleBlock(sText);

      if (bFound)
      {
        m_eBlockType = EBlockType::None;
      }
    }

    setCurrentBlockState(static_cast<int>(m_eBlockType));

    m_eBlockType = EBlockType::None;
  }
}

void CSyntaxHighlighter::HighlightSingleBlock(const QString &sText)
{
  switch(m_eBlockType)
  {
  case EBlockType::SingleLineComment:
  case EBlockType::MultiLineComment:
    setFormat(m_iBlockPosition, m_iBlockLength, m_formatComment);
    break;

  case EBlockType::SingleQuoteString:
  case EBlockType::DoubleQuoteString:
    setFormat(m_iBlockPosition, m_iBlockLength, m_formatString);
    break;

  default:
    break;
  }

  m_iBlockPosition += m_iBlockLength;
}

void CSyntaxHighlighter::FindBlockStart(const QString &sText)
{
  // find first occurance of all block identifiers
  int iSingleLineComment = sText.indexOf(c_liBlockIdentifier[static_cast<int>(EBlockType::SingleLineComment)], m_iBlockPosition, Qt::CaseSensitive);
  int iMultiLineComment = sText.indexOf(c_liBlockIdentifier[static_cast<int>(EBlockType::MultiLineComment)], m_iBlockPosition, Qt::CaseSensitive);
  int iDoubleQuote = sText.indexOf(c_liBlockIdentifier[static_cast<int>(EBlockType::DoubleQuoteString)], m_iBlockPosition, Qt::CaseSensitive);
  int iSingleQuote = sText.indexOf(c_liBlockIdentifier[static_cast<int>(EBlockType::SingleQuoteString)], m_iBlockPosition, Qt::CaseSensitive);

  // determine which block appears first
  m_iBlockPosition = iSingleLineComment;
  m_eBlockType = EBlockType::SingleLineComment;

  if ((iMultiLineComment > -1 && iMultiLineComment < m_iBlockPosition) ||
      m_iBlockPosition == -1)
  {
    m_iBlockPosition = iMultiLineComment;
    m_eBlockType = EBlockType::MultiLineComment;
  }

  if ((iDoubleQuote > -1 && iDoubleQuote < m_iBlockPosition) ||
      m_iBlockPosition == -1)
  {
    m_iBlockPosition = iDoubleQuote;
    m_eBlockType = EBlockType::DoubleQuoteString;
  }

  if ((iSingleQuote > -1 && iSingleQuote < m_iBlockPosition) ||
      m_iBlockPosition == -1)
  {
    m_iBlockPosition = iSingleQuote;
    m_eBlockType = EBlockType::SingleQuoteString;
  }

  if (m_iBlockPosition == -1)
  {
    // set block type to none, if we did not find any identifier
    m_eBlockType = EBlockType::None;
    m_iBlockPosition = sText.size();
  }
}

bool CSyntaxHighlighter::FindBlockEnd(const QString &sText)
{
  bool bFound = false;
  int iPositionEnd = 0;

  // search for block end identifier
  // escaped quotes will be skipped, so we have to loop
  while (!bFound)
  {
    iPositionEnd = sText.indexOf(c_liBlockIdentifierEnd[static_cast<int>(m_eBlockType)],
        m_iBlockPosition + c_liBlockIdentifier[static_cast<int>(m_eBlockType)].size() + iPositionEnd,
        Qt::CaseSensitive);

    if (iPositionEnd == -1)
    {
      // we reached the end of the string
      // set current block state accordingly
      iPositionEnd = sText.size();
      break;
    }
    else
    {
      bFound = true;

      // a escaped single/double quote is not valid, so skip it
      if (m_eBlockType == EBlockType::DoubleQuoteString ||
          m_eBlockType == EBlockType::SingleQuoteString)
      {
        if (sText[iPositionEnd - 1] == '\\')
        {
          bFound = false;
        }
      }
    }
  }

  m_iBlockLength = iPositionEnd - m_iBlockPosition + c_liBlockIdentifierEnd[static_cast<int>(m_eBlockType)].size();

  return bFound;
}

}
