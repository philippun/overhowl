/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CController.h"
#include <QApplication>
#include <QMainWindow>
#include <QAction>

using namespace Overhowl;

class Application : public QApplication
{
public:
  Application(int& argc, char *argv[])
    : QApplication(argc, argv)
  {

  }

  bool notify(QObject *o, QEvent *e) override
  {
    try
    {
      return QApplication::notify(o, e);
    }
    catch (...)
    {
      return false;
    }
  }
};

int main(int argc, char *argv[])
{
  Application a(argc, argv);

#ifdef _WIN32
  // use Humanity icon theme provided as default
  // Windows has no icon themes
  QIcon::setThemeName("Humanity-provided");
#endif

  CController oContr;

  return a.exec();
}
