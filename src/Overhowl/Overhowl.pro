#-------------------------------------------------
#
# Project created by QtCreator 2018-08-19T00:02:43
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = Overhowl
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES = $$files(*.cpp, true)

INCLUDEPATH += $$PWD/../DataModel \
               $$PWD/../ScriptEngine \
               $$PWD/../Gui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DESTDIR = ../build
QMAKE_RPATHDIR += ./

win32: LIBS += -L$$OUT_PWD/../build/ -lController
else:unix: LIBS += -L$$OUT_PWD/../build/ -lController

INCLUDEPATH += $$PWD/../Controller
DEPENDPATH += $$PWD/../Controller

win32: LIBS += -L$$OUT_PWD/../build/ -lGui
else:unix: LIBS += -L$$OUT_PWD/../build/ -lGui

INCLUDEPATH += $$PWD/../Gui
DEPENDPATH += $$PWD/../Gui

win32: LIBS += -L$$OUT_PWD/../build/ -lScriptEngine
else:unix: LIBS += -L$$OUT_PWD/../build/ -lScriptEngine

INCLUDEPATH += $$PWD/../ScriptEngine
DEPENDPATH += $$PWD/../ScriptEngine

win32: LIBS += -L$$OUT_PWD/../build/ -lDataModel
else:unix: LIBS += -L$$OUT_PWD/../build/ -lDataModel

INCLUDEPATH += $$PWD/../DataModel
DEPENDPATH += $$PWD/../DataModel
