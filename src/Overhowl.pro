TEMPLATE = subdirs

SUBDIRS = \
  Controller \
  DataModel \
  Gui \
  Overhowl \
  ScriptEngine \
  PluginCommon \
  PluginTesting

Controller.depends = Gui ScriptEngine DataModel
Gui.depends = DataModel
Overhowl.depends = Controller
PluginCommon.depends = Controller
PluginTesting.depends = Controller
