/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CDialog.h"

#include "CTestingDialog.h"
#include "IScriptEngine.h"
#include "CExtensionLoader.h"

#include <QThread>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QJSEngine>
#include <QCoreApplication>
#include <QEventLoop>

#include <QMessageBox>

namespace Overhowl {
namespace Testing {

CDialog::CDialog()
  : IScriptClass("Dialog")
{
  this->moveToThread(QCoreApplication::instance()->thread());
}

QJSValue CDialog::YesOrNo(QString sTitle, QString sText)
{
  InitDialog(sTitle, sText, EType::YesOrNo);
  return ShowDialog();
}

QJSValue CDialog::MultipleTestSteps(QString sTest, QString sDescription, QJSValue jTests)
{
  InitDialog(sTest, sDescription, EType::MultipleTests, jTests);
  return ShowDialog();
}

ScriptEngine::IScriptClass* CDialog::GetInstance(void)
{
  return new CDialog();
}

void CDialog::ShowDialogSlot()
{
  switch (m_eType)
  {
    case EType::YesOrNo:
    {
      m_jResult = QJSValue(QMessageBox::question(nullptr, m_sTitle, m_sText, QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes);
    }
      break;

    case EType::MultipleTests:
    {
      if (CTestingDialog* dialog = new CTestingDialog())
      {
        dialog->moveToThread(QCoreApplication::instance()->thread()); //@todo parameters to get the title, desc, tests
        dialog->setWindowTitle(m_sTitle);
        dialog->SetDescription(m_sText);
        if (dialog->FillTests(m_jTests.toVariant()))
        {
          if (dialog->exec() == QDialog::Accepted)
          {
             m_jResult = ScriptEngine::CExtensionLoader::Engine()->toScriptValue(dialog->GetResults());
          }
        }
        delete dialog;
      }
    }
      break;
  }

  emit DialogClosed();
}

void CDialog::InitDialog(QString sTitle, QString sText, CDialog::EType eType, QJSValue jTests)
{
  m_sTitle = sTitle;
  m_sText = sText;
  m_eType = eType;
  m_jTests = jTests;
  m_jResult = QJSValue();
}

QJSValue CDialog::ShowDialog()
{
  if (QMetaObject::invokeMethod(this, "ShowDialogSlot"))
  {
    QEventLoop loop;
    connect(this, &CDialog::DialogClosed, &loop, &QEventLoop::quit);
    loop.exec();
  }

  m_sTitle.clear();
  m_sText.clear();
  m_jTests = QJSValue();

  return m_jResult;
}

}
}
