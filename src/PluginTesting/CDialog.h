/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CDIALOG_H
#define CDIALOG_H

#include "IScriptClass.h"

#include <QJSValue>

namespace Overhowl {
namespace Testing {

class CDialog : public ScriptEngine::IScriptClass
{
  Q_OBJECT

public:
  enum class EType
  {
    YesOrNo,
    MultipleTests
  };

public:
  CDialog();
  ~CDialog(void) = default;

  Q_INVOKABLE QJSValue YesOrNo(QString sTitle, QString sText);
  Q_INVOKABLE QJSValue MultipleTestSteps(QString sTest, QString sDescription, QJSValue jTests);

  // IScriptClass
  IScriptClass* GetInstance(void);


signals:
  void DialogClosed();


private slots:
  void ShowDialogSlot();


private:
  void InitDialog(QString sTitle, QString sText, EType eType, QJSValue jTests = QJSValue());
  QJSValue ShowDialog();


private:
  QString m_sTitle;
  QString m_sText;
  EType m_eType;
  QJSValue m_jTests;
  QJSValue m_jResult;
};

}
}

#endif // CDIALOG_H
