/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CDIALOGEXTENSION_H
#define CDIALOGEXTENSION_H

#include "IPlugin.h"
#include "CResultsModel.h"
#include "CDockWidgetResults.h"

namespace Overhowl {
namespace Testing {

class CPlugin : public IPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "Testing")

public:
  CPlugin();

  // IScriptExtension
  QString PluginName() override
  {
    return "Testing";
  }
  QList<ScriptEngine::IScriptClass*> GetScriptClasses() override;
  QList<QString> GetScriptFunctions() override;
  void SelectedItemChanged(int iID) override;
  void SessionClosed() override;
  void SessionOpened(QString sName) override;
  QList<QPair<EPosition, QDockWidget*>> GetDockWidgets() override;

  void AddResult(QString sName, QString sValue);


private:
  int m_iSelectedItemID = -1;
  CDockWidgetResults* m_dockResults = nullptr;
  CResultsModel* m_model = nullptr;
};

}
}

#endif // CDIALOGEXTENSION_H
