/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CTesting.h"
#include "CDialog.h"
#include "CResultsInterface.h"
#include "IScriptEngine.h"

#include <QJSEngine>

namespace Overhowl {
namespace Testing {

CPlugin::CPlugin()
{
  m_dockResults = new CDockWidgetResults();
  m_model = new CResultsModel();

  m_dockResults->GetResults()->setModel(m_model);
  CResultsInterface::m_results = m_model;
}

QList<ScriptEngine::IScriptClass*> CPlugin::GetScriptClasses()
{
  return { new CDialog(), new CResultsInterface() };
}

QList<QString> CPlugin::GetScriptFunctions()
{
  return
  {
    "var __results_interface__ = new _results_interface_();",
    "function AddResult(name, value) { __results_interface__.AddResult(name, value); }"
  };
}

void CPlugin::SelectedItemChanged(int iID)
{
  m_iSelectedItemID = iID;

  if (m_model)
  {
    m_model->UnderlyingItemChanged(iID);
  }
}

void CPlugin::SessionClosed()
{
  m_model->Serialize();
}

void CPlugin::SessionOpened(QString sName)
{
  m_model->Deserialize();
}

QList<QPair<EPosition, QDockWidget*> > CPlugin::GetDockWidgets()
{
  return { {EPosition::Right, m_dockResults} };
}

void CPlugin::AddResult(QString sName, QString sValue)
{
  QJSEngine* pEngine = qjsEngine(this);
  int iID = dynamic_cast<ScriptEngine::IScriptEngine*> (pEngine)->GetCurrentScriptID();

  m_model->AddResult({sName, "", sValue}, iID);
}

}
}
