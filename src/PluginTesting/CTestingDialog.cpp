/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CTestingDialog.h"
#include "ui_TestingDialog.h"

#include <QHBoxLayout>

CTestingDialog::CTestingDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::CTestingDialog)
{
  ui->setupUi(this);

  m_list = new QListWidget();
  ui->widgetTests->setLayout(new QHBoxLayout());
  ui->widgetTests->layout()->addWidget(m_list);
}

CTestingDialog::~CTestingDialog()
{
  delete ui;
}

void CTestingDialog::SetDescription(QString desc)
{
  ui->labelDescription->setText(desc);
}

bool CTestingDialog::FillTests(QVariant tests)
{
  if (tests.type() == QVariant::Type::List)
  {
    QList<QVariant> list = tests.toList();
    for(QVariant& var : list)
    {
      FillTest(var);
    }
  }
  else if (tests.type() == QVariant::Type::Map)
  {
    FillTest(tests);
  }

  return true;
}

QVariant CTestingDialog::GetResults()
{
  QMap<QString, QVariant> map;
  for (int i = 0; i < m_list->count(); ++i)
  {
    map[m_list->item(i)->data(Qt::UserRole).toString()] = (m_list->item(i)->checkState() == Qt::Checked ? "success" : "failure");
  }

  return QVariant(map);
}

bool CTestingDialog::FillTest(QVariant& var)
{
  if (var.type() == QVariant::Type::Map)
  {
    QMap<QString, QVariant> map = var.toMap();
    QListWidgetItem* item = new QListWidgetItem();
    item->setCheckState(Qt::Unchecked);
    item->setText(map["test"].toString());
    item->setData(Qt::ItemDataRole::UserRole, map["test"].toString());
    m_list->addItem(item);
  }

  return true;
}
