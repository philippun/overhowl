/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CDOCKWIDGETRESULTS_H
#define CDOCKWIDGETRESULTS_H

#include "CTableViewResults.h"

#include <QDockWidget>
#include <QAction>

class QTextEdit;

namespace Ui {
class CDockWidgetResults;
}

//class QTableView;

namespace Overhowl {
namespace Testing {

class CDockWidgetResults : public QDockWidget
{
  Q_OBJECT

public:
  explicit CDockWidgetResults(QWidget *parent = nullptr);
  ~CDockWidgetResults();

  CTableViewResults* GetResults();


private slots:
  void UpdateToolbar(bool bSelected);


private:
  Ui::CDockWidgetResults *ui;

  QAction* m_up = nullptr;
  QAction* m_down = nullptr;
  QAction* m_add = nullptr;
  QAction* m_remove = nullptr;
};

}
}

#endif // CDOCKWIDGETRESULTS_H
