/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CTableViewResults_H
#define CTableViewResults_H

#include <QTableView>


namespace Overhowl {

namespace DataModel {
  class CItemScript;
}

namespace Testing {

/**
 * @brief The CTableViewResults class
 */
class CTableViewResults : public QTableView
{
  Q_OBJECT

public:
  CTableViewResults(QWidget* parent = nullptr);

public slots:
  void ItemDoubleClicked(const QModelIndex& index);
  void UpResult();
  void DownResult();
  void AddResult();
  void RemoveResult();

signals:
  void SelectionChanged(bool bSelected);


private:
  void selectionChanged(const QItemSelection& selected, const QItemSelection& deselected) override;
  void mousePressEvent(QMouseEvent* event) override;

  void Move(int direction);
};

}
}

#endif // CTableViewResults_H
