/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CDockWidgetResults.h"
#include "ui_DockWidgetResults.h"

#include <QToolBar>

namespace Overhowl {
namespace Testing {

CDockWidgetResults::CDockWidgetResults(QWidget *parent) :
  QDockWidget(parent),
  ui(new Ui::CDockWidgetResults)
{
  ui->setupUi(this);

  QToolBar* tools = new QToolBar();
  tools->setIconSize(QSize(16, 16));

  m_up = new QAction();
  m_up->setIcon(QIcon::fromTheme("up"));
  tools->addAction(m_up);

  m_down = new QAction();
  m_down->setIcon(QIcon::fromTheme("down"));
  tools->addAction(m_down);

  m_add = new QAction();
  m_add->setIcon(QIcon::fromTheme("add"));
  m_add->setEnabled(true);
  tools->addAction(m_add);

  m_remove = new QAction();
  m_remove->setIcon(QIcon::fromTheme("remove"));
  tools->addAction(m_remove);

  ui->verticalLayout->insertWidget(0, tools);
  UpdateToolbar(false);

  connect(ui->tableViewResults, &CTableViewResults::SelectionChanged, this, &CDockWidgetResults::UpdateToolbar);
  connect(m_up, &QAction::triggered, ui->tableViewResults, &CTableViewResults::UpResult);
  connect(m_down, &QAction::triggered, ui->tableViewResults, &CTableViewResults::DownResult);
  connect(m_add, &QAction::triggered, ui->tableViewResults, &CTableViewResults::AddResult);
  connect(m_remove, &QAction::triggered, ui->tableViewResults, &CTableViewResults::RemoveResult);
}

CDockWidgetResults::~CDockWidgetResults()
{
  delete ui;
}

CTableViewResults* CDockWidgetResults::GetResults()
{
  return ui->tableViewResults;
}

void CDockWidgetResults::UpdateToolbar(bool bSelected)
{
  m_up->setEnabled(bSelected);
  m_down->setEnabled(bSelected);
  m_remove->setEnabled(bSelected);
}

}
}
