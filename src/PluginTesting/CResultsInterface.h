/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CRESULTSINTERFACE_H
#define CRESULTSINTERFACE_H

#include "IScriptClass.h"

#include <QObject>

namespace Overhowl {
namespace Testing {

class CResultsModel;

class CResultsInterface final : public ScriptEngine::IScriptClass
{
  friend class CPlugin;
  Q_OBJECT

public:
  CResultsInterface();

  IScriptClass* GetInstance(void) override;

  Q_INVOKABLE void AddResult(QString sName, QString sValue);


private:
  static CResultsModel* m_results;
};

}
}

#endif // CRESULTSINTERFACE_H
