/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CRESULTSMODEL_H
#define CRESULTSMODEL_H

#include <QAbstractTableModel>

namespace Overhowl {
namespace Testing {

struct SResult
{
  QString sName;
  QString sExpectedValue;
  QString sValue;
};

struct SItemResult
{
  int iID;
  QList<SResult> liResults;
};

/**
 * @brief The CResultsModel class
 */
class CResultsModel : public QAbstractTableModel
{
  Q_OBJECT

public:
  CResultsModel(QObject* parent = nullptr);

  void AddResult(SResult result, int iID, bool bNew = false);
  void RemoveResult(QModelIndex& index);
  QModelIndex Move(QModelIndex& index, int direction);
  void ResetResults(int iID);
  void Clear();

  void Serialize();
  void Deserialize();

  // QAbstractItemModel
  int columnCount(const QModelIndex& parent) const override;
  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;
  QVariant data(const QModelIndex& index, int role) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;


private:
  SItemResult* GetItemByID(int iID);
  SResult& MakeUnique(SResult& result, SItemResult& item);
  void ReaddResult(SResult result, int row);


signals:
  void Modified();


public slots:
  void UnderlyingItemChanged(int iID);


private:
  enum eColumns
  {
    NAME,
    VALUE,
    EXPECTED_VALUE,
    COLUMNS
  };

  SItemResult* m_underlyingItem = nullptr;
  int m_iID = -1;
  QList<SItemResult*> m_liResults;
};

}
}
#endif // CRESULTSMODEL_H
