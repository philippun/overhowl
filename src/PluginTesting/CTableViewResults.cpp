/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CTableViewResults.h"
#include "CResultsModel.h"

#include <QHeaderView>
#include <QClipboard>
#include <QApplication>
#include <QMouseEvent>

namespace Overhowl {
namespace Testing {

CTableViewResults::CTableViewResults(QWidget* parent)
  : QTableView(parent)
{
  setSelectionBehavior(QAbstractItemView::SelectRows);
  setSelectionMode(QAbstractItemView::SingleSelection);
  horizontalHeader()->setStretchLastSection(true);
  verticalHeader()->hide();

  connect (this, &CTableViewResults::doubleClicked, this, &CTableViewResults::ItemDoubleClicked);
}

void CTableViewResults::ItemDoubleClicked(const QModelIndex& index)
{
  QApplication::clipboard()->setText(index.data().toString());
}

void CTableViewResults::UpResult()
{
  Move(-1);
}

void CTableViewResults::DownResult()
{
  Move(1);
}

void CTableViewResults::AddResult()
{
  auto resultsModel = static_cast<CResultsModel*>(model());
  resultsModel->AddResult({"new","",""}, -1, true);
}

void CTableViewResults::RemoveResult()
{
  auto selectedModel = selectionModel();
  auto selected = selectedModel->selectedRows();

  if (selected.size() == 1)
  {
    if (auto resultsModel = static_cast<CResultsModel*>(model()))
    {
      resultsModel->RemoveResult(selected[0]);
    }
  }
}

void CTableViewResults::selectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
  emit SelectionChanged(selected.size() > 0);

  QTableView::selectionChanged(selected, deselected);
}

void CTableViewResults::mousePressEvent(QMouseEvent* event)
{
  // unselect if mouse was clicked inside empty tableview part
  QModelIndex item = indexAt(event->pos());
  if (item.row() == -1 && item.column() == -1)
  {
    clearSelection();
    QModelIndex index;
    selectionModel()->setCurrentIndex(index, QItemSelectionModel::Select);
  }

  QTableView::mousePressEvent(event);
}

void CTableViewResults::Move(int direction)
{
  auto selectedModel = selectionModel();
  auto selected = selectedModel->selectedRows();

  if (selected.size() == 1)
  {
    if (auto resultsModel = static_cast<CResultsModel*>(model()))
    {
      auto newSelected = resultsModel->Move(selected[0], direction);
      selectedModel->setCurrentIndex(newSelected, QItemSelectionModel::SelectionFlag::Select | QItemSelectionModel::SelectionFlag::Rows);
    }
  }
}

}
}
