/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CResultsModel.h"

#include <QBrush>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

namespace Overhowl {
namespace Testing {

CResultsModel::CResultsModel(QObject* parent)
  : QAbstractTableModel(parent)
{

}

void CResultsModel::AddResult(SResult result, int iID, bool bNew)
{
  bool bExists = false;

  if (iID == -1 && m_underlyingItem != nullptr)
  {
    iID = m_underlyingItem->iID;
  }

  if (m_underlyingItem != nullptr && iID == m_underlyingItem->iID)
  {
    beginResetModel();
  }

  auto item = GetItemByID(iID);
  if (item == nullptr)
  {
    item = new SItemResult();
    item->iID = iID;
    m_liResults.push_back(item);
  }

  if (item != nullptr)
  {
    if (bNew)
    {
      MakeUnique(result, *item);
      bExists = false;
    }
    else
    {
      for (auto& res : item->liResults)
      {
        if (res.sName == result.sName)
        {
          res.sValue = result.sValue;
          bExists = true;
        }
      }
    }

    if (bExists == false)
    {
      item->liResults.push_back(result);
      // refresh underlying item, because it might just have been added with this new result
      UnderlyingItemChanged(m_iID);
    }
  }
  emit Modified();

  if ((m_underlyingItem != nullptr && iID == m_underlyingItem->iID) || bExists == false)
  {
    endResetModel();
  }
}

void CResultsModel::RemoveResult(QModelIndex& index)
{
  if (m_underlyingItem)
  {
    beginRemoveRows(index.parent(), index.row(), index.row());
    m_underlyingItem->liResults.removeAt(index.row());
    endRemoveRows();
  }
}

QModelIndex CResultsModel::Move(QModelIndex& index, int direction)
{
  if (m_underlyingItem)
  {
    int row = index.row();
    auto result = m_underlyingItem->liResults[row];

    if (row + direction < m_underlyingItem->liResults.size()
        && row + direction >= 0)
    {
      RemoveResult(index);
      ReaddResult(result, row + direction);

      return createIndex(row + direction, 0, nullptr);
    }
  }

  return QModelIndex();
}

void CResultsModel::ResetResults(int iID)
{
  if (auto item = GetItemByID(iID))
  {
    item->liResults.clear();
    emit Modified();
  }
}

void CResultsModel::Clear()
{
  beginResetModel();
  m_underlyingItem = nullptr;
  endResetModel();
}

void CResultsModel::Serialize()
{
  QJsonObject jObj;
  QJsonArray jItemResults;

  for (auto item : m_liResults)
  {
    QJsonObject jItem;
    jItem.insert("id", item->iID);

    QJsonArray jResults;

    for (auto result : item->liResults)
    {
      jResults.push_back(QJsonObject({
                             {"name", result.sName},
                             {"value", result.sValue},
                             {"expected", result.sExpectedValue}
                           }));
    }

    jItem.insert("results", jResults);
    jItem.insert("id", item->iID);

    jItemResults.push_back(jItem);
  }

  jObj.insert("results", jItemResults);

  QJsonDocument jDoc(jObj);

  // save it to file @todo use real paths (i.e. programdata, .local/share)
#if __linux__
  QFile file("/home/phil/results.json");
#endif

#if _WIN32
  QFile file("results.json");
#endif
  file.open(QIODevice::WriteOnly | QIODevice::Text);
  if (file.isOpen())
  {
    file.write(jDoc.toJson());
    //m_model->DataSaved();
  }

  Clear();
}

void CResultsModel::Deserialize()
{
  // read file and parse the json data @todo use real paths (i.e. programdata, .local/share)
#if __linux__
  QFile file("/home/phil/results.json");
#endif

#if _WIN32
  QFile file("results.json");
#endif
  QString content;
  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    content = file.readAll();
  }

  QJsonParseError err;
  QJsonDocument jDoc = QJsonDocument::fromJson(content.toUtf8(), &err);

  QJsonObject jObj = jDoc.object();

  if (jObj.value("results").isUndefined() == false)
  {
    QJsonArray jItems = jObj.value("results").toArray();

    for (int i = 0; i < jItems.size(); ++i)
    {
      SItemResult* result = new SItemResult();
      result->iID = jItems[i].toObject().value("id").toInt();
      QJsonArray jResults = jItems[i].toObject().value("results").toArray();

      for (int j = 0; j < jResults.size(); ++j)
      {
        result->liResults.push_back(
              SResult{
                jResults[j].toObject().value("name").toString(),
                jResults[j].toObject().value("expected").toString(),
                jResults[j].toObject().value("value").toString()
              });
      }

      m_liResults.push_back(result);
    }
  }
}

int CResultsModel::columnCount(const QModelIndex& parent) const
{
  return COLUMNS;
}

int CResultsModel::rowCount(const QModelIndex& parent) const
{
  if (m_underlyingItem == nullptr)
  {
    return 0;
  }

  return m_underlyingItem->liResults.count();
}

Qt::ItemFlags CResultsModel::flags(const QModelIndex& index) const
{
  if (index.row() < m_underlyingItem->liResults.count())
  {
    // rows of non supplemental results have special flags
    // value column just needs to be selectable
    if (index.column() == VALUE)
    {
      return QAbstractTableModel::flags(index) | Qt::ItemIsSelectable;
    }
    // all other columns must be editable (includes selectable)
    else
    {
      return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
    }
  }

  return QAbstractItemModel::flags(index);
}

QVariant CResultsModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid() || m_underlyingItem == nullptr)
  {
    return QModelIndex();
  }

  // get data to be displayed
  if (role == Qt::DisplayRole)
  {
    if (index.column() == NAME)
    {
      return m_underlyingItem->liResults.at(index.row()).sName;
    }
    else if (index.column() == VALUE)
    {
      return m_underlyingItem->liResults.at(index.row()).sValue;
    }
    else if (index.column() == EXPECTED_VALUE)
    {
      return m_underlyingItem->liResults.at(index.row()).sExpectedValue;
    }
  }
  else if (role == Qt::EditRole)
  {
    if (index.row() < m_underlyingItem->liResults.count())
    {
      if (index.column() == NAME)
      {
        return m_underlyingItem->liResults.at(index.row()).sName;
      }
      else if (index.column() == EXPECTED_VALUE)
      {
        return m_underlyingItem->liResults.at(index.row()).sExpectedValue;
      }
    }
  }
  // change the background color if the expected value and actual value do or do not match
  // green: values match
  // red:   values mismatch
  // gray:  no value set
  else if (role == Qt::BackgroundRole)
  {
    if (index.row() < m_underlyingItem->liResults.count())
    {
      if (index.column() == VALUE)
      {
        if (m_underlyingItem->liResults.at(index.row()).sValue.isEmpty())
        {
          return QBrush(Qt::GlobalColor::gray);
        }
        else if (m_underlyingItem->liResults.at(index.row()).sValue == m_underlyingItem->liResults.at(index.row()).sExpectedValue)
        {
          return QBrush(Qt::GlobalColor::green);
        }
        else
        {
          return QBrush(Qt::GlobalColor::red);
        }
      }
    }
  }

  return QVariant();
}

QVariant CResultsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  // table header
  if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
  {
    if (section == NAME)
    {
      return QVariant("Name");
    }
    else if (section == VALUE)
    {
      return QVariant("Result");
    }
    else if (section == EXPECTED_VALUE)
    {
      return QVariant("Expected");
    }
  }

  return QVariant();
}

bool CResultsModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
  if (index.row() < m_underlyingItem->liResults.count())
  {
    if (index.column() == NAME)
    {
      m_underlyingItem->liResults[index.row()].sName = value.toString();
    }
    else if (index.column() == EXPECTED_VALUE)
    {
      m_underlyingItem->liResults[index.row()].sExpectedValue = value.toString();
    }

    emit Modified();

    return true;
  }

  return false;
}

SItemResult* CResultsModel::GetItemByID(int iID)
{
  for (auto item : m_liResults)
  {
    if (item->iID == iID)
    {
      return item;
    }
  }

  auto item = new SItemResult();
  item->iID = iID;
  m_liResults.push_back(item);
  return item;
}

SResult& CResultsModel::MakeUnique(SResult& result, SItemResult& item)
{
  bool bFound = true;
  size_t zSuffix = 0;
  QString sName = result.sName;

  while (bFound)
  {
    bFound = false;
    ++zSuffix;

    for (auto existing : item.liResults)
    {
      if (result.sName == existing.sName)
      {
        bFound = true;
        result.sName = sName + QString::number(zSuffix);
        break;
      }
    }
  }

  return result;
}

void CResultsModel::ReaddResult(SResult result, int row)
{
  if (m_underlyingItem)
  {
    beginResetModel();
    m_underlyingItem->liResults.insert(row, result);
    endResetModel();
  }
}

void CResultsModel::UnderlyingItemChanged(int iID)
{
  m_iID = iID;

  //@todo update the view when the script execution starts, probably needs to be done in the gui state machine
  beginResetModel();
  m_underlyingItem = GetItemByID(iID);
  endResetModel();
}

}
}
