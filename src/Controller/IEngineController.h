/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IENGINECONTROLLER_H
#define IENGINECONTROLLER_H

#include <SBreakpoint.h>

#include <QString>

namespace Overhowl {

class IEngineController
{
public:
  virtual ~IEngineController() = default;

  virtual void RunScript(QString sScript, QString sFilename, int iID) = 0;
  virtual void Run() = 0;
  virtual void Stop() = 0;
  virtual void Pause() = 0;
  virtual void StepOver() = 0;
  virtual void StepInto() = 0;
  virtual void StepOut() = 0;
  virtual bool IsRunning() = 0;
  virtual bool IsPaused() = 0;
  virtual QString GetCurrentlyRunningScript() = 0;
  virtual void AddBreakpoint(SBreakpoint breakpoint) = 0;
  virtual void RemoveBreakpoint(SBreakpoint breakpoint) = 0;
};

}

#endif // IENGINECONTROLLER_H
