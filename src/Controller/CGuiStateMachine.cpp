/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CGuiStateMachine.h"
#include "CMainWindow.h"
#include "IEngineController.h"

#include <QThread>
#include <QTimer>

namespace Overhowl {

using namespace DataModel;

CGuiStateMachine::CGuiStateMachine(CMainWindow* mainWindow)
  : m_mainWindow(mainWindow)
{
  // the gui will be updated by the state machine every 50ms
  m_thread = new QThread;
  m_thread->start();
  this->moveToThread(m_thread);

  m_timer = new QTimer();
  m_timer->setInterval(50);

  m_timer->start();

  connect(m_timer, &QTimer::timeout, this, &CGuiStateMachine::UpdateGui);
}

void CGuiStateMachine::SetEngine(IEngineController* engine)
{
  m_engine = engine;
}

void CGuiStateMachine::CalculateGuiStates()
{
  // configure debugging actions
  if (m_bProjectRunning || m_bScriptRunning)
  {
    m_bState_Action_StopScript = true;
    m_bState_Action_RunScript = m_bDebuggerPaused;
    m_bState_Action_PauseScript = !m_bDebuggerPaused;
    m_bState_Action_StepOver = m_bDebuggerPaused;
    m_bState_Action_StepInto = m_bDebuggerPaused;
    m_bState_Action_StepOut = m_bDebuggerPaused;

    m_bState_Action_NewProject = false;
    m_bState_Action_NewScript = false;

    m_bState_Action_RunScriptFromProject = false;
    m_bState_Action_RunProject = false;
    m_bState_Action_RunAllProjects = false;

    m_bState_Action_NewSession = false;
    m_bState_Action_OpenSession = false;
    m_bState_Action_CloseSession = false;
  }
  else
  {
    // calculate project actions state
    switch (m_eSessionItemType)
    {
    // folder not implemented now @pending
    //  // folder can have script children
    //case CSessionItem::EType::Folder:
    //  m_bState_Action_NewProject = false;
    //  m_bState_Action_NewScript = true;
    //  break;
      // session (main node) can have project children
    case CSessionItem::EType::Session:
      m_bState_Action_NewProject = true;
      m_bState_Action_NewScript = false;
      break;

      // projects can have script or folder children
    case CSessionItem::EType::Project:
      m_bState_Action_NewProject = false;
      m_bState_Action_NewScript = true;
      break;

      // scripts must not have children items
    case CSessionItem::EType::Script:
      // no selection, so disable all toolbuttons
    case CSessionItem::EType::None:
    default:
      m_bState_Action_NewProject = false;
      m_bState_Action_NewScript = false;
      break;
    }

    // calculate debugger actions state
    // states depending on selected session item
    switch (m_eSessionItemType)
    {
    default:
    case CSessionItem::EType::None:
      m_bState_Action_RunScriptFromProject = false;
      m_bState_Action_RunProject = false;
      break;

    case CSessionItem::EType::Session:
      m_bState_Action_RunScriptFromProject = false;
      m_bState_Action_RunProject = false;
      break;

    case CSessionItem::EType::Script:
      m_bState_Action_RunScriptFromProject = true;
      m_bState_Action_RunProject = false;
      break;

    case CSessionItem::EType::Project:
      m_bState_Action_RunScriptFromProject = false;
      m_bState_Action_RunProject = true;
      break;
    }
    // other debugger states
    m_bState_Action_RunScript = m_bScriptOpen;
    m_bState_Action_RunAllProjects = true;
    m_bState_Action_StopScript = false;
    m_bState_Action_PauseScript = false;
    m_bState_Action_StepOver = false;
    m_bState_Action_StepInto = false;
    m_bState_Action_StepOut = false;

    // overwrite all debugger states if no session is loaded
    if (m_bSessionLoaded == false)
    {
      m_bState_Action_RunScriptFromProject = false;
      m_bState_Action_RunProject = false;
      m_bState_Action_RunAllProjects = false;
      m_bState_Action_StopScript = false;
      m_bState_Action_PauseScript = false;
      m_bState_Action_StepInto = false;
      m_bState_Action_StepOver = false;
      m_bState_Action_StepOut = false;
    }

    // new/open/close session states
    m_bState_Action_NewSession = true;
    m_bState_Action_OpenSession = true;
    m_bState_Action_CloseSession = true;
    if (m_bSessionLoaded == false)
    {
      m_bState_Action_CloseSession = false;
    }
  }
}

void CGuiStateMachine::UpdateGui()
{
  // get information from engine if available
  if (m_engine)
  {
    m_bScriptRunning = m_engine->IsRunning();
    m_bDebuggerPaused = m_engine->IsPaused();

    if (m_bScriptRunning == false || (m_bScriptRunning == false && m_bDebuggerPaused == false))
    {
      m_debugPosition.first = "";
      m_debugPosition.second = 0;
    }
  }

  // calculate gui states from all available variables
  CalculateGuiStates();

  // notify the gui about the newly calculated states
  emit UpdateDebugActions(m_bState_Action_RunScript, m_bState_Action_RunScriptFromProject, m_bState_Action_RunProject, m_bState_Action_RunAllProjects, //@todo run script must not be always true, check if a script is opened
                          m_bState_Action_StopScript, m_bState_Action_PauseScript, m_bState_Action_StepOver, m_bState_Action_StepInto, m_bState_Action_StepOut);
  emit UpdateProjectActions(m_bState_Action_NewScript, m_bState_Action_NewProject);
  emit UpdateDebuggingPosition(m_debugPosition.first, m_debugPosition.second);
  emit UpdateSessionActions(m_bState_Action_NewSession, m_bState_Action_OpenSession, m_bState_Action_CloseSession);
  emit UpdateVariablesView(m_bScriptRunning);
}

void CGuiStateMachine::DebuggerStartedSlot()
{
  m_bScriptRunning = true;
}

void CGuiStateMachine::DebuggerPausedSlot(QString sFile, int iLine)
{
  m_debugPosition.first = sFile;
  m_debugPosition.second = iLine;
  m_bDebuggerPaused = true;
}

void CGuiStateMachine::DebuggerResumedSlot()
{
  m_debugPosition.first = "";
  m_debugPosition.second = 0;
  m_bDebuggerPaused = false;
}

void CGuiStateMachine::DebuggerFinishedSlot()
{
  m_debugPosition.first = "";
  m_debugPosition.second = 0;
  m_bDebuggerPaused = false;
  m_bScriptRunning = false;
}

void CGuiStateMachine::ProjectSelectionChangedType(int eType)
{
  m_eSessionItemType = static_cast<CSessionItem::EType> (eType);
}

void CGuiStateMachine::ScriptOpenSlot(bool bOpen)
{
  m_bScriptOpen = bOpen;
}

void CGuiStateMachine::SessionLoadedSlot(QString sSession, bool bLoaded)
{
  m_bSessionLoaded = bLoaded;
}

}
