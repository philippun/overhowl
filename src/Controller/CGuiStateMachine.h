/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CGUISTATEMACHINE_H
#define CGUISTATEMACHINE_H

#include "CSessionItem.h"
#include "CMainWindow.h"
#include "IEngineController.h"

#include <QObject>

// forward declarations
class QThread;
class QTimer;


namespace Overhowl {

class CGuiStateMachine : public QObject
{
  Q_OBJECT

public:
  CGuiStateMachine(CMainWindow* mainWindow);

  void SetEngine(IEngineController* engine);
  void CalculateGuiStates();


public slots:
  void UpdateGui();
  void DebuggerStartedSlot();
  void DebuggerPausedSlot(QString sFile, int iLine);
  void DebuggerResumedSlot();
  void DebuggerFinishedSlot();
  void ProjectSelectionChangedType(int eType);
  void ScriptOpenSlot(bool bOpen);
  void SessionLoadedSlot(QString sSession, bool bLoaded);


signals:
  void UpdateDebugActions(bool bRunScript, bool bRunScriptFromProject, bool bRunProject, bool bRunAllProjects,
                          bool bStop, bool bPause, bool bStepOver, bool bStepInto, bool bStepOut);
  void UpdateProjectActions(bool bNewScript, bool bNewProject);
  void UpdateDebuggingPosition(QString sFile, int iLine);
  void UpdateSessionActions(bool bNew, bool bOpen, bool bClose);
  void UpdateVariablesView(bool bEnabled);


private:
  // state triggers
  bool m_bScriptRunning = false;
  bool m_bProjectRunning = false;
  bool m_bDebuggerPaused = false;
  bool m_bScriptOpen = false;
  bool m_bSessionLoaded = false;
  QPair<QString, int> m_debugPosition = { "", 0 };
  DataModel::CSessionItem::EType m_eSessionItemType = DataModel::CSessionItem::EType::None;

  // states
  bool m_bState_Action_NewProject = false;
  bool m_bState_Action_NewScript = false;

  bool m_bState_Action_RunScript = false;
  bool m_bState_Action_RunScriptFromProject = false;
  bool m_bState_Action_RunProject = false;
  bool m_bState_Action_RunAllProjects = false;
  bool m_bState_Action_StopScript = false;
  bool m_bState_Action_PauseScript = false;
  bool m_bState_Action_StepInto = false;
  bool m_bState_Action_StepOver = false;
  bool m_bState_Action_StepOut = false;

  bool m_bState_Action_NewSession = false;
  bool m_bState_Action_OpenSession = false;
  bool m_bState_Action_CloseSession = false;


  CMainWindow* m_mainWindow = nullptr;
  IEngineController* m_engine = nullptr;

  QThread* m_thread = nullptr;
  QTimer* m_timer = nullptr;
};

}

#endif // CGUISTATEMACHINE_H
