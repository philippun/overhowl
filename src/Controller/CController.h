/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CCONTROLLER_H
#define CCONTROLLER_H

#include "controller_global.h"

#include <QObject>
#include <QTimer>
#include <QThread>

#include "CPlugins.h"
#include "CItemScript.h"
#include "IEngineController.h"
#include "IGui.h"

namespace Overhowl {

// forward declarations
namespace ScriptEngine {
  class CEngineController;
}
namespace DataModel {
  class CMainModel;
  class CBreakpoints;
  class CSerializer;
}
class CMainWindow;
class CGui;
class CGuiStateMachine;

class CONTROLLERSHARED_EXPORT CController : public QObject
{
  Q_OBJECT

public:
  CController();
  ~CController();

  void Startup();
  void Shutdown();
  void CloseSession();

  IEngineController& Engine();
  IGui& Gui();


public slots:
  void Control();
  void ProjectSelectionChangedItem(int iID);
  void CloseFile(bool bSave);
  void SessionLoaded(QString sSession, bool bLoaded);
  void ActivatePlugin(SPlugin plugin);


private:
  void ConnectGui();
  void ConnectEngine();
  void ConnectGuiStateController();


public:
  static QString sAppData;

private:
  DataModel::CPlugins* m_plugins = nullptr;
  ScriptEngine::CEngineController* m_engine = nullptr;
  DataModel::CMainModel* m_mainModel = nullptr;
  DataModel::CBreakpoints* m_breakpointsModel = nullptr;
  DataModel::CSerializer* m_serializer = nullptr;
  CMainWindow* m_mainWindow = nullptr;
  CGui* m_gui = nullptr;
  CGuiStateMachine* m_guiStateController = nullptr;

  QThread* m_thread = nullptr;
};

}

#endif // CCONTROLLER_H
