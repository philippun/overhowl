/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CONTROLLER_GLOBAL_H
#define CONTROLLER_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CONTROLLER_LIBRARY) || defined(Controller_EXPORTS)
#  define CONTROLLERSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CONTROLLERSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CONTROLLER_GLOBAL_H
