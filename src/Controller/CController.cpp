/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CController.h"
#include "CMainWindow.h"

#include "CMainWindow.h"
#include "CMainModel.h"
#include "CGui.h"
#include "CBreakpoints.h"
#include "CEngineController.h"
#include "CExtensionLoader.h"
#include "CGuiStateMachine.h"
#include "CSerializer.h"
#include "TextEditor/CTabWidgetFiles.h"

#include <QString>
#include <QAction>
#include <QMessageBox>
#include <QStandardPaths>
#include <QDir>
#include <QApplication>

using namespace Overhowl::ScriptEngine;
using namespace Overhowl::DataModel;

namespace Overhowl {

QString CController::sAppData;

CController::CController()
{
  qRegisterMetaType<SPlugin>("SPlugin");
  QTimer::singleShot(0, this, &CController::Startup);
}

CController::~CController()
{
  Shutdown();
}

void CController::Startup()
{
  sAppData = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation)[0];
  if (QDir().exists(sAppData) == false)
  {
    QDir().mkdir(sAppData);
  }

  // plugin loader
  m_plugins = new CPlugins();
  m_plugins->LoadPlugins();
  connect(m_plugins, &CPlugins::ActivatePlugin, this, &CController::ActivatePlugin);

  // create all models
  m_mainModel = &CMainModel::Instance(this);
  //m_mainModel->moveToThread(m_thread);
  m_breakpointsModel = new CBreakpoints(this);

  //create engine
  m_engine = new CEngineController(m_breakpointsModel, m_plugins);
  m_gui = new CGui(m_engine);
  m_engine->SetGuiObject(m_gui);

  m_breakpointsModel->SetEngineController(m_engine);

  // data serialzer
  m_serializer = new DataModel::CSerializer(m_mainModel);

  // create main window
  m_mainWindow = new CMainWindow();
  m_mainWindow->SetupWindow(*m_engine, *m_mainModel, *m_breakpointsModel, *m_plugins);

  // Activate plugins
  auto plugins = m_plugins->GetPlugins();
  for (auto plugin : plugins)
  {
    ActivatePlugin(plugin);
  }
  ScriptEngine::CExtensionLoader::SetMainwindow(m_mainWindow);

  // gui state controller
  m_guiStateController = new CGuiStateMachine(m_mainWindow);
  m_guiStateController->SetEngine(m_engine);

  // connect components
  ConnectGui();
  ConnectEngine();
  ConnectGuiStateController();

  // move controller into its own thread (after the gui was connected in the main thread
  m_thread = new QThread();
  m_thread->start();
  this->moveToThread(m_thread);
}

void CController::Shutdown()
{
  m_thread->exit();
  while (m_thread->isRunning())
  {
    QThread::msleep(20);
  }
  delete m_thread;

  m_mainWindow->UnsetWindow();

  if (m_mainModel)
  {
    delete m_mainModel;
  }

  if (m_plugins)
  {
    delete m_plugins;
  }
}

IEngineController& CController::Engine()
{
  return *m_engine;
}

IGui& CController::Gui()
{
  return *m_gui;
}

void CController::Control()
{

}

void CController::ProjectSelectionChangedItem(int iID)
{
  for (auto plugin : m_plugins->GetPlugins())
  {
    plugin.plugin->SelectedItemChanged(iID);
  }
}

void CController::CloseFile(bool bSave)
{
  m_serializer->CloseFile(bSave);

  m_plugins->SessionClosed();
}

void CController::SessionLoaded(QString sSession, bool bLoaded)
{
  if (bLoaded)
  {
    m_plugins->SessionOpened(sSession);
  }
}

void CController::ActivatePlugin(SPlugin plugin)
{
  if (plugin.bLoad)
  {
    m_mainWindow->AddDockWidgets(plugin.plugin->GetDockWidgets());
  }
  else
  {
    m_mainWindow->RemoveDockWidgets(plugin.plugin->GetDockWidgets());
  }
}

void CController::ConnectGui()
{
  // gui output
  connect(m_gui, &CGui::SignalWriteLog, m_mainWindow, &CMainWindow::SlotWriteLog);
  connect(m_gui, &CGui::SignalWriteOutput, m_mainWindow, &CMainWindow::SlotWriteOutput);
  connect(m_gui, &CGui::SignalWriteTrace, m_mainWindow, &CMainWindow::SlotWriteTrace);
  connect(m_gui, &CGui::SignalShowStatusBarMessage, m_mainWindow, &CMainWindow::SlotShowStatusBarMessage);

  // gui state machine -> gui
  connect(m_guiStateController, &CGuiStateMachine::UpdateDebugActions, m_mainWindow, &CMainWindow::UpdateDebugActions);
  connect(m_guiStateController, &CGuiStateMachine::UpdateProjectActions, m_mainWindow, &CMainWindow::UpdateProjectActions);
  connect(m_guiStateController, &CGuiStateMachine::UpdateDebuggingPosition, m_mainWindow, &CMainWindow::UpdateDebuggingPositionSlot);
  connect(m_guiStateController, &CGuiStateMachine::UpdateSessionActions, m_mainWindow, &CMainWindow::UpdateSessionActionsSlot);
  connect(m_guiStateController, &CGuiStateMachine::UpdateVariablesView, m_mainWindow, &CMainWindow::UpdateVariablesViewSlot);

  // session actions
  connect(m_mainWindow, &CMainWindow::NewSession, m_serializer, &CSerializer::NewFile);
  connect(m_mainWindow, &CMainWindow::OpenSession, m_serializer, &CSerializer::LoadFile);
  connect(m_mainWindow, &CMainWindow::CloseSession, this, &CController::CloseFile);

  // expand all after session file load
  connect(m_serializer, &CSerializer::FileLoaded, m_mainWindow->m_project, &CTreeViewProject::expandAll);

  // session/script info for gui state machine
  connect(m_serializer, &CSerializer::SessionLoaded, m_guiStateController, &CGuiStateMachine::SessionLoadedSlot);
  connect(m_mainWindow->m_files, &CTabWidgetFiles::ScriptOpen, m_guiStateController, &CGuiStateMachine::ScriptOpenSlot);

  connect(m_serializer, &CSerializer::SessionLoaded, m_mainWindow->m_files, &CTabWidgetFiles::SessionLoadedSlot);
  connect(m_serializer, &CSerializer::SessionLoaded, m_mainWindow, &CMainWindow::SessionLoadedSlot);
  connect(m_serializer, &CSerializer::SessionLoaded, this, &CController::SessionLoaded);
}

void CController::ConnectEngine()
{
  // debugging actions (stepping, etc.)
  connect(m_mainWindow->m_actionPause_Script, &QAction::triggered, m_engine, &CEngineController::Pause);
  connect(m_mainWindow->m_actionResume_Script, &QAction::triggered, m_engine, &CEngineController::Run);
  connect(m_mainWindow->m_actionStep_Over, &QAction::triggered, m_engine, &CEngineController::StepOver);
  connect(m_mainWindow->m_actionStep_Into, &QAction::triggered, m_engine, &CEngineController::StepInto);
  connect(m_mainWindow->m_actionStep_Out, &QAction::triggered, m_engine, &CEngineController::StepOut);

  // update locals gui
  connect(m_engine, &CEngineController::LocalsUpdated, m_mainWindow, &CMainWindow::LocalsUpdatedSlot);
}

void CController::ConnectGuiStateController()
{
  // debugger events
  connect(m_engine, &CEngineController::DebuggerStarted, m_guiStateController, &CGuiStateMachine::DebuggerStartedSlot);
  connect(m_engine, &CEngineController::DebuggerPaused, m_guiStateController, &CGuiStateMachine::DebuggerPausedSlot);
  connect(m_engine, &CEngineController::DebuggerFinished, m_guiStateController, &CGuiStateMachine::DebuggerFinishedSlot);
  connect(m_engine, &CEngineController::DebuggerResumed, m_guiStateController, &CGuiStateMachine::DebuggerResumedSlot);

  // projects
  connect(m_mainWindow, &CMainWindow::SelectionChangedType, m_guiStateController, &CGuiStateMachine::ProjectSelectionChangedType);
  connect(m_mainWindow, &CMainWindow::SelectionChangedItem, this, &CController::ProjectSelectionChangedItem);
}

}
