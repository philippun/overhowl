/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IPLUGIN_H
#define IPLUGIN_H

#include "controller_global.h"

#include "IScriptClass.h"

class QDockWidget;

namespace Overhowl {

enum class EPosition
{
  Left,
  Right,
  Bottom
};

class CONTROLLERSHARED_EXPORT IPlugin : public QObject
{
  Q_OBJECT

public:
  virtual ~IPlugin() = default;

  virtual QString PluginName() = 0;
  virtual QList<ScriptEngine::IScriptClass*> GetScriptClasses() = 0;
  virtual QList<QString> GetScriptFunctions() { return {}; }

  virtual QList<QPair<EPosition, QDockWidget*>> GetDockWidgets() { return {}; }


public slots:
  virtual void SelectedItemChanged(int /*iID*/) {}
  virtual void SessionClosed() {}
  virtual void SessionOpened(QString /*sName*/) {}
};

}

#endif // IPLUGIN_H
