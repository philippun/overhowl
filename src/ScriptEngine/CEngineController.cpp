/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CEngineController.h"
#include "CScriptEngine.h"
#include "CScriptDebugger.h"
#include "CExtensionLoader.h"
#include "IScriptClass.h"

#include <QFile>
#include <QJSValue>


namespace Overhowl {
namespace ScriptEngine {

CEngineController::CEngineController(IBreakpoints* breakPoints, IPlugins* plugins)
  : m_breakPoints(breakPoints)
  , m_plugins(plugins)
  , m_engineLock(QMutex::Recursive)
{
  IScriptClass::SetWaitCondition(&m_engineSleepCondition, &m_engineSleepLock);
}

CEngineController::~CEngineController()
{
  QMutexLocker locker(&m_engineLock);
  StopEngine();

  if (m_engineThread)
  {
    delete m_engineThread;
    m_engineThread = nullptr;
  }
}

void CEngineController::SetGuiObject(IGui* gui)
{
  m_gui = gui;

  IScriptClass::SetGui(m_gui);
}

void CEngineController::RunScript(QString sScript, QString sFilename, int iID)
{
  QMutexLocker locker(&m_engineLock);
  StartupEngine();

  if (m_debugger && m_engine)
  {
    m_debugger->AddBreakpoints(m_breakPoints->GetBreakpoints(), sFilename);

    auto plugins = m_plugins->GetPlugins();
    for (const SPlugin& plugin : plugins)
    {
      if (plugin.bLoad)
      {
        LoadPlugin(plugin.plugin);
      }
    }

    emit ExecuteScript(sScript, sFilename, iID);
  }
}

void CEngineController::Run()
{
  QMutexLocker locker(&m_engineLock);
  if (m_debugger)
  {
    m_debugger->Continue();
  }
}

void CEngineController::Stop()
{
  QMutexLocker locker(&m_engineLock);
  if (m_debugger)
  {
    StopEngine();
    emit DebuggerFinished();
  }
}

void CEngineController::Pause()
{
  QMutexLocker locker(&m_engineLock);
  if (m_debugger)
  {
    m_debugger->Pause();
  }
}

void CEngineController::StepOver()
{
  QMutexLocker locker(&m_engineLock);
  if (m_debugger)
  {
    m_debugger->StepOver();
  }
}

void CEngineController::StepInto()
{
  QMutexLocker locker(&m_engineLock);
  if (m_debugger)
  {
    m_debugger->StepInto();
  }
}

void CEngineController::StepOut()
{
  QMutexLocker locker(&m_engineLock);
  if (m_debugger)
  {
    m_debugger->StepOut();
  }
}

bool CEngineController::IsRunning()
{
  QMutexLocker locker(&m_engineLock);
  if (m_engine)
  {
    return m_engine->IsRunning();
  }

  return false;
}

bool CEngineController::IsPaused()
{
  QMutexLocker locker(&m_engineLock);
  if (m_engine)
  {
    return m_engine->IsPaused();
  }

  return false;
}

QString CEngineController::GetCurrentlyRunningScript()
{
  QMutexLocker locker(&m_engineLock);
  return m_engine->GetCurrentScript();
}

void CEngineController::AddBreakpoint(SBreakpoint breakpoint)
{
  QMutexLocker locker(&m_engineLock);
  if (m_debugger)
  {
    m_debugger->AddBreakpoint(breakpoint, m_engine->GetCurrentScript());
  }
}

void CEngineController::RemoveBreakpoint(SBreakpoint breakpoint)
{
  QMutexLocker locker(&m_engineLock);
  if (m_debugger)
  {
    m_debugger->RemoveBreakpoint(breakpoint, m_engine->GetCurrentScript());
  }
}

void CEngineController::WriteLog(QString sText)
{
  if (m_gui)
  {
    m_gui->WriteLog(sText);
  }
}

void CEngineController::DebuggerFinishedSlot(QString sResult)
{
  m_bRunning = false;
  m_bPaused = false;

  if (m_gui)
  {
    if (sResult == "true")
    {
      m_gui->ShowStatusBarMessage("success");
    }
    else
    {
      m_gui->ShowStatusBarMessage("failure");
    }
  }
}

void CEngineController::StartupEngine()
{
  QMutexLocker locker(&m_engineLock);
  StopEngine();

  m_engineThread = new QThread();
  m_engineThread->setObjectName("engine thread");
  m_engine = new CScriptEngine();
  m_debugger = new CScriptDebugger(m_engine);
  m_engine->SetDebugger(m_debugger);
  m_engine->moveToThread(m_engineThread);

  connect(this, &CEngineController::ExecuteScript, m_engine, &CScriptEngine::ExecuteScript);
  connect(m_engine, &CScriptEngine::ExecutionStarted, this, &CEngineController::DebuggerStarted);
  connect(m_engine, &CScriptEngine::ExecutionFinished, this, &CEngineController::DebuggerFinished);
  connect(m_engine, &CScriptEngine::ExecutionFinished, this, &CEngineController::DebuggerFinishedSlot);
  connect(m_engine, &CScriptEngine::WriteLog, this, &CEngineController::WriteLog);
  connect(m_debugger, &CScriptDebugger::DebuggerPaused, this, &CEngineController::DebuggerPaused);
  connect(m_debugger, &CScriptDebugger::LocalsUpdated, this, &CEngineController::LocalsUpdated);
  connect(m_debugger, &CScriptDebugger::DebuggerResumed, this, &CEngineController::DebuggerResumed);

  //ScriptExtension::CExtensionLoader extender(m_engine);
  QJSValue factoryObject = m_engine->newQObject(new CExtensionLoader(m_engine));
  QString sErr = factoryObject.toString();
  m_engine->globalObject().setProperty(QString("_customClassFactory"), factoryObject);

  m_engineThread->start();
}

void CEngineController::StopEngine()
{
  QMutexLocker locker(&m_engineLock);

  if (m_debugger)
  {
    m_engineSleepLock.lock();
    m_debugger->Pause();
    m_engineSleepCondition.wakeAll();

    QThread::msleep(10);

    m_engineSleepLock.unlock();
  }

#ifdef _WIN32
  if (m_engine)
  {
    delete m_engine;
    m_engine = nullptr;
    // debugger will be deleted by the engine
    m_debugger = nullptr;
  }
#endif

  if (m_engineThread)
  {
    int i = 0;
    while(m_engineThread->isFinished() == false)
    {
      m_engineSleepLock.lock();
      m_engineThread->exit();
      m_engineThread->wait();
      m_engineSleepLock.unlock();

      QThread::msleep(10);

      if (i++ > 100)
      {
        break;
      }
    }

    if (m_engineThread->isFinished())
    {
      delete m_engineThread;
    }
    else
    {
      m_engineThread->terminate();
      m_engineThread->wait();
      if (m_engineThread->isFinished())
      {
        delete m_engineThread;
      }
      else
      {
        m_engineThread->deleteLater(); // must never happen, the application will crash!
      }
    }
    m_engineThread = nullptr;
  }

#ifdef __linux__
  if (m_engine)
  {
    delete m_engine;
    m_engine = nullptr;
    // debugger will be deleted by the engine
    m_debugger = nullptr;
  }
#endif
}

bool CEngineController::LoadPlugin(IPlugin* plugin)
{
  bool bReturn = true;

  auto classes = plugin->GetScriptClasses();
  for (ScriptEngine::IScriptClass* cls : classes)
  {
    CExtensionLoader::AddClass(cls);

    QString sLoader = "function " + cls->GetName() + "() {"
                      "return _customClassFactory.CreateClassInstance(\"" + cls->GetName() + "\");"
                      "}";
    QJSValue val = m_engine->evaluate(sLoader);

    if (val.isError())
    {
      //m_gui.WriteLog("Error while adding class " + classObject->GetName() + ": " + val.toString()); //@todo log error
      bReturn = false;
    }
  }

  auto functions = plugin->GetScriptFunctions();
  for (const QString& sFunctionString : functions)
  {
    QJSValue val = m_engine->evaluate(sFunctionString);

     if (val.isError())
     {
       QString sErr = val.toString();
       //Gui().WriteLog("Error while adding functions in class " + QString::fromLatin1(this->metaObject()->className()).split("::").last() + ": " + val.toString()); //@todo log error
       bReturn = false;
     }
  }

  return bReturn;
}

}
}
