/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CExtensionLoader.h"
#include "CScriptEngine.h"

namespace Overhowl {
namespace ScriptEngine {

QList<IScriptClass*> CExtensionLoader::m_liClasses;
ScriptEngine::CScriptEngine* CExtensionLoader::m_engine = nullptr;
QMainWindow* CExtensionLoader::m_mainwindow = nullptr;

CExtensionLoader::CExtensionLoader(CScriptEngine* engine)
{
  m_engine = engine;
}

void CExtensionLoader::AddClass(IScriptClass* cls)
{
  m_liClasses.append(cls);
}

void CExtensionLoader::ClearClasses()
{
  for (IScriptClass* cls : m_liClasses)
  {
    if (cls != nullptr)
    {
      delete cls;
    }
  }
  m_liClasses.clear();

  m_engine = nullptr;
}

QJSEngine* CExtensionLoader::Engine()
{
  return m_engine;
}

QMainWindow* CExtensionLoader::Mainwindow()
{
  return m_mainwindow;
}

void CExtensionLoader::SetMainwindow(QMainWindow* window)
{
  m_mainwindow = window;
}

QJSValue CExtensionLoader::CreateClassInstance(QString sClassName)
{
  if (IScriptClass* pClass = FindClass(sClassName))
  {
    return m_engine->newQObject(pClass);
  }

  return QJSValue();
}

IScriptClass* CExtensionLoader::FindClass(QString sClassName)
{
  for (IScriptClass* pClass : m_liClasses)
  {
    if (pClass->GetName() == sClassName)
    {
      return pClass->GetInstance();
    }
  }

  return nullptr;
}

}
}
