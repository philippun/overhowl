/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CScriptDebugger.h"
#include "CScriptEngine.h"


namespace Overhowl {
namespace ScriptEngine {

CScriptDebugger::CScriptDebugger(CScriptEngine* engine)
  : QV4Debugger(engine->v4Engine())
  , m_collector(engine->v4Engine())
  , m_engine(engine)
{
  connect(this, &CScriptDebugger::debuggerPaused, this, &CScriptDebugger::DebuggerPausedSlot);
}

void CScriptDebugger::AddBreakpoints(QVector<SBreakpoint> liBreakpoints, QString sFilename)
{
  for (SBreakpoint breakpoint : liBreakpoints)
  {
    if (breakpoint.m_bActive)
    {
      AddBreakpoint(breakpoint, sFilename);
    }
  }
}

void CScriptDebugger::AddBreakpoint(SBreakpoint breakpoint, QString sFilename)
{
  addBreakPoint(breakpoint.m_sFilename,
                breakpoint.m_iLineNumber + ((sFilename == breakpoint.m_sFilename) ? 1 : 0)); // add one line for the __main_function__
}

void CScriptDebugger::RemoveBreakpoint(SBreakpoint breakpoint, QString sFilename)
{
  removeBreakPoint(breakpoint.m_sFilename,
                   breakpoint.m_iLineNumber + ((sFilename == breakpoint.m_sFilename) ? 1 : 0)); // add one line for the __main_function__
}

void CScriptDebugger::Continue(void)
{
  Resume(Speed::FullThrottle);
}

void CScriptDebugger::Pause()
{
  pause();
}

void CScriptDebugger::StepOver()
{
  Resume(Speed::StepOver);
}

void CScriptDebugger::StepInto()
{
  Resume(Speed::StepIn);
}

void CScriptDebugger::StepOut()
{
  Resume(Speed::StepOut);
}

void CScriptDebugger::DebuggerPausedSlot(QV4Debugger *debugger, QV4Debugger::PauseReason reason)
{
  m_bPaused = true;

  m_stackTrace = debugger->stackTrace();

  if (m_stackTrace.size() > 0)
  {
    auto file = m_stackTrace.first().source;
    if (file.startsWith("file://"))
    {
      file = file.mid(7);
    }
    m_sDebuggedFile = file;
    m_iDebuggedLine = m_stackTrace.first().line;
    auto line = m_iDebuggedLine - (m_sDebuggedFile == m_engine->GetCurrentScript() ? 1 : 0);
    emit DebuggerPaused(m_sDebuggedFile, line);
  }

  // we have to skip the last line of our main script, which is just a "}" to define our __main_function__ scope
  if (m_engine->GetCurrentScript() == m_sDebuggedFile && m_engine->GetCurrentLineCount() < m_iDebuggedLine)
  {
    resume(Speed::FullThrottle);
    return;
  }

  for (int i = 0, ei = m_stackTrace.size(); i != ei; ++i)
  {
    ScopeJob job(&m_collector, i, 0);
    debugger->runInEngine(&job);
    QJsonObject object = job.returnValue();
    object = object.value(QLatin1String("object")).toObject();

    if (object.contains("ref") && !object.contains("properties"))
    {
      object = LookupRef(object.value("ref").toInt());
    }

    QList<SVariable> liLocals;
    ParseStack(object, liLocals);

    // signal the locals of the current stack frame
    if (i == 0)
    {
      emit LocalsUpdated(liLocals);
    }
  }
}

void CScriptDebugger::ParseStack(QJsonObject& object, QList<SVariable>& liLocals)
{
  foreach (const QJsonValue &value, object.value(QLatin1String("properties")).toArray())
  {
    QJsonObject property = value.toObject();

    if (property["type"].toString() == "function")
    {
      continue;
    }

    SVariable variable = SVariable{property["name"].toString(), "", property["type"].toString()};

    if (property["type"].toString() == "object" && property.contains("ref"))
    {
      QJsonObject subObject = LookupRef(property["ref"].toInt());

      ParseStack(subObject, variable.Children);
    }

    // at the moment only string, numeric and boolean parametrs can be showed in the variable table
    if (property["value"].isString())
    {
      variable.Value = property["value"].toString();
    }
    else if (property["value"].isDouble())
    {
      variable.Value = QString::number(property["value"].toDouble());
    }
    else if (property["value"].isBool())
    {
      variable.Value = property["value"].toBool() ? "true" : "false";
    }

    liLocals.append(variable);
}
}

QJsonObject CScriptDebugger::LookupRef(uint ref)
{
#if QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 9
  return m_collector.lookupRef(ref, true);
#elif QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 12
  return m_collector.lookupRef(ref);
#endif
}

void CScriptDebugger::Resume(QV4Debugger::Speed speed)
{
  resume(speed);

  if (m_engine->IsRunning())
  {
    m_bPaused = false;
    emit DebuggerResumed();
  }
}

}
}
