/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ISCRIPTCLASS_H
#define ISCRIPTCLASS_H

#include "scriptengine_global.h"

#include "IGui.h"

#include <QString>
#include <QObject>

class QMutex;
class QWaitCondition;

namespace Overhowl {
namespace ScriptEngine {

class SCRIPTENGINESHARED_EXPORT IScriptClass : public QObject
{
  Q_OBJECT
  friend class CEngineController;

public:
  IScriptClass(QString sName, QObject* parent = nullptr);
  virtual ~IScriptClass();

  virtual IScriptClass* GetInstance(void) = 0;
  QString GetName(void);


protected:
  IGui& Gui();
  void Sleep(int iTime);


private:
  static void SetGui(IGui* gui);
  static void SetWaitCondition(QWaitCondition* condition, QMutex* mutex);


private:
  QString m_sClassName;

  // interfaces to the gui environment
  static IGui* m_gui;
  static QWaitCondition* m_condition;
  static QMutex* m_mutex;
};

}
}

#endif // ISCRIPTCLASS_H
