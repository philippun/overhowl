/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CEXTENSIONLOADER_H
#define CEXTENSIONLOADER_H

#include "scriptengine_global.h"

#include "IScriptClass.h"

#include <QObject>
#include <QJSValue>
#include <QJSEngine>
#include <QMainWindow>

namespace Overhowl {
namespace ScriptEngine {
class CScriptEngine;

class SCRIPTENGINESHARED_EXPORT CExtensionLoader : public QObject
{
  Q_OBJECT

public:
  CExtensionLoader(CScriptEngine* engine);

  static void AddClass(IScriptClass* cls);
  static void ClearClasses();
  static QJSEngine* Engine();
  static QMainWindow* Mainwindow();
  static void SetMainwindow(QMainWindow* window);

  Q_INVOKABLE QJSValue CreateClassInstance(QString sClassName);


private:
  IScriptClass* FindClass(QString sClassName);


private:
  static QList<IScriptClass*> m_liClasses;
  static ScriptEngine::CScriptEngine* m_engine;
  static QMainWindow* m_mainwindow;
};

}
}

#endif // CEXTENSIONLOADER_H
