/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CSCRIPTENGINE_H
#define CSCRIPTENGINE_H

#include <QString>
#include <QJSEngine>

#if QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 9
#include <qtdebugger5.9/qv4debugger.h>
#include <qtdebugger5.9/qv4datacollector.h>
#elif QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 12
#include <qtdebugger5.12/qv4debugger.h>
#include <qtdebugger5.12/qv4datacollector.h>
#endif
#include <private/qv4global_p.h>
#include <private/qv4engine_p.h>
#include <private/qv4debugging_p.h>
#include <private/qv8engine_p.h>

#include "IScriptEngine.h"

// forward declarations
class QThread;

namespace Overhowl {
namespace ScriptEngine {

// forward declarations
class CScriptDebugger;


/**
 * @brief The CScriptEngine class
 */
class CScriptEngine : public QJSEngine, public IScriptEngine
{
  Q_OBJECT
  friend class CScriptDebugger;
  friend class CEngineController;

public:
  CScriptEngine();
  virtual ~CScriptEngine() override;

  void SetDebugger(CScriptDebugger* debugger);
  QString GetCurrentScript() override;
  int GetCurrentScriptID() override;
  int GetCurrentLineCount() override;
  bool IsRunning()
  {
    return m_bRunning;
  }
  bool IsPaused();


public slots:
  void ExecuteScript(QString sScript, QString sFilename, int iID);


signals:
  void ExecutionStarted();
  void ExecutionFinished(QString sResult);
  void WriteLog(QString sText);
  void EvaluationFinished();


private:
  QV4::ExecutionEngine* v4Engine();


private:
  CScriptDebugger*                        m_debugger = nullptr;
  QString                                 m_sScript;
  int                                     m_iScriptID = -1;
  int                                     m_iLines = 0;
  bool                                    m_bRunning = false;
};

}
}

#endif // CSCRIPTENGINE_H
