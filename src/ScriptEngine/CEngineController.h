/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CENGINECONTROLLER_H
#define CENGINECONTROLLER_H

#include "scriptengine_global.h"

#include "IEngineController.h"
#include "IScriptClass.h"
#include "IPlugins.h"
#include "IGui.h"
#include "IBreakpoints.h"
#include "SVariable.h"

#include <QObject>
#include <QMutex>
#include <QWaitCondition>
#include <QThread>


namespace Overhowl {
namespace ScriptEngine {

// forward declarations
class CScriptEngine;
class CScriptDebugger;


/**
 * @brief The CEngineController class
 */
class SCRIPTENGINESHARED_EXPORT CEngineController : public QObject, public IEngineController
{
  Q_OBJECT

public:
  CEngineController(IBreakpoints* breakPoints, IPlugins* plugins);
  ~CEngineController() override;

  void SetGuiObject(IGui* gui);

  // IEngineController
  void RunScript(QString sScript, QString sFilename, int iID) override;
  void Run() override;
  void Stop() override;
  void Pause() override;
  void StepOver() override;
  void StepInto() override;
  void StepOut() override;
  bool IsRunning() override;
  bool IsPaused() override;
  QString GetCurrentlyRunningScript() override;
  void AddBreakpoint(SBreakpoint breakpoint) override;
  void RemoveBreakpoint(SBreakpoint breakpoint) override;


public slots:
  void WriteLog(QString sText);
  void DebuggerFinishedSlot(QString sResult);


signals:
  void ExecuteScript(QString sScript, QString sFilename, int iID);
  void DebuggerStarted();
  void DebuggerPaused(QString sFile, int iLine);
  void DebuggerFinished();
  void DebuggerResumed();
  void LocalsUpdated(QList<SVariable> liLocals);


private:
  void StartupEngine();
  void StopEngine();
  bool LoadPlugin(IPlugin* plugin);


private:
  QThread* m_engineThread = nullptr;
  CScriptEngine* m_engine = nullptr;
  CScriptDebugger* m_debugger = nullptr;
  IBreakpoints* m_breakPoints = nullptr;
  IGui* m_gui = nullptr;
  IPlugins* m_plugins = nullptr;

  bool m_bRunning = false;
  bool m_bPaused = false;

  QMutex m_engineLock;
  QMutex m_engineSleepLock;
  QWaitCondition m_engineSleepCondition;
};

}
}

#endif // CENGINECONTROLLER_H
