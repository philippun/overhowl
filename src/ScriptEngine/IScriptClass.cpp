/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "IScriptClass.h"

#include <QWaitCondition>
#include <QMutex>

namespace Overhowl {
namespace ScriptEngine {

IGui* IScriptClass::m_gui = nullptr;
QWaitCondition* IScriptClass::m_condition = nullptr;
QMutex* IScriptClass::m_mutex = nullptr;

IScriptClass::IScriptClass(QString sName, QObject* parent)
  : QObject(parent)
  , m_sClassName(sName)

{

}

IScriptClass::~IScriptClass() = default;

QString IScriptClass::GetName()
{
  return m_sClassName;
}

IGui&IScriptClass::Gui()
{
  return *m_gui;
}

void IScriptClass::Sleep(int iTime)
{
  m_mutex->lock();
  m_condition->wait(m_mutex, iTime);
  m_mutex->unlock();
}

void IScriptClass::SetGui(IGui* gui)
{
  m_gui = gui;
}

void IScriptClass::SetWaitCondition(QWaitCondition* condition, QMutex* mutex)
{
  m_condition = condition;
  m_mutex = mutex;
}

}
}
