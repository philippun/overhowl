/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CScriptEngine.h"
#include "IGui.h"
#include "CScriptDebugger.h"
#include "CExtensionLoader.h"

#include <QtGlobal>
#include <QThread>
#if QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 9
#include <private/qv4isel_moth_p.h>
#include <qtdebugger5.9/qv4debugjob.h>
#elif QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 12
#include <qtdebugger5.12/qv4debugjob.h>
#endif


namespace Overhowl {
namespace ScriptEngine {


CScriptEngine::CScriptEngine()
  : QJSEngine()
{
}

CScriptEngine::~CScriptEngine()
{
  ScriptEngine::CExtensionLoader::ClearClasses();
}

void CScriptEngine::SetDebugger(CScriptDebugger* debugger)
{
  m_debugger = debugger;
  v4Engine()->setDebugger(debugger);
#if QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 9
  v4Engine()->iselFactory.reset(new QV4::Moth::ISelFactory);
#endif
}

QString CScriptEngine::GetCurrentScript()
{
  return m_sScript;
}

int CScriptEngine::GetCurrentScriptID()
{
  return m_iScriptID;
}

int CScriptEngine::GetCurrentLineCount()
{
  return m_iLines;
}

bool CScriptEngine::IsPaused()
{
  if (m_debugger)
  {
    return m_debugger->IsPaused();
  }

  return false;
}

void CScriptEngine::ExecuteScript(QString sScript, QString sFilename, int iID)
{
  QString sResult = "true";
  m_sScript = sFilename;
  m_iScriptID = iID;

  sScript = "__main_function__();function __main_function__(){\n" + sScript + "\n}";
  m_iLines = sScript.split("\n").count() - 1; // skip the last line

  emit ExecutionStarted();
  m_bRunning = true;
  QJSValue oVal = QJSEngine::evaluate(sScript, sFilename);
  m_bRunning = false;

  if (oVal.isError())
  {
    emit WriteLog("Error in " + oVal.property("fileName").toString() + " line " + oVal.property("lineNumber").toString() + ": " + oVal.toString());
    sResult = "false";
  }
  else
  {
    emit WriteLog("Execution successful");
  }

  m_sScript.clear();

  emit ExecutionFinished(sResult);
}

QV4::ExecutionEngine* CScriptEngine::v4Engine()
{
#if QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 9
  return QV8Engine::getV4(this);
#elif QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 12
  return handle();
#else
  return nullptr;
#endif
}

}
}
