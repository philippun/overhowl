/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CSCRIPTDEBUGGER_H
#define CSCRIPTDEBUGGER_H

#include "SBreakpoint.h"
#include "SVariable.h"
#include "CScriptEngine.h"

#if QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 9
#include <qtdebugger5.9/qv4debugger.h>
#include <qtdebugger5.9/qv4debugjob.h>
#elif QT_VERSION_MAJOR == 5 && QT_VERSION_MINOR == 12
#include <qtdebugger5.12/qv4debugger.h>
#include <qtdebugger5.12/qv4debugjob.h>
#endif
#include <QVector>


namespace Overhowl {
namespace ScriptEngine {


/**
 * @brief The CScriptDebugger class
 */
class CScriptDebugger : public QV4Debugger
{
  friend class CScriptEngine;
  Q_OBJECT

public:
  CScriptDebugger(CScriptEngine* engine);
  ~CScriptDebugger() = default;

  void AddBreakpoints(QVector<SBreakpoint> liBreakpoints, QString sFilename);
  void AddBreakpoint(SBreakpoint breakpoint, QString sFilename);
  void RemoveBreakpoint(SBreakpoint breakpoint, QString sFilename);

  void Continue();
  void Pause();
  void StepOver();
  void StepInto();
  void StepOut();

  bool IsPaused()
  {
    return m_bPaused;
  }


public slots:
  void DebuggerPausedSlot(QV4Debugger *debugger, QV4Debugger::PauseReason reason);


signals:
  void DebuggerPaused(QString sFile, int iLine);
  void DebuggerResumed();
  void LocalsUpdated(QList<SVariable> liLocals);


private:
  void Resume(Speed speed);
  void ParseStack(QJsonObject& object, QList<SVariable>& liLocals);
  QJsonObject LookupRef(uint ref);


private:
  QVector<QV4::StackFrame>  m_stackTrace;
  QV4DataCollector          m_collector;
  CScriptEngine*            m_engine;

  QString                   m_sDebuggedFile;
  int                       m_iDebuggedLine = 0;

  bool                      m_bPaused = false;
};

}
}

#endif // CSCRIPTDEBUGGER_H
