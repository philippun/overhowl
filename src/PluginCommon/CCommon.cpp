/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CCommon.h"

#include "IScriptEngine.h"
#include "IGui.h"

#include <QThread>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QJSEngine>

namespace Overhowl {
namespace ScriptEngine {

CCommon::CCommon()
  : IScriptClass("Common")
{

}

void CCommon::Print(QString sMsg)
{
  Gui().WriteOutput(sMsg);
}

void CCommon::Trace(QString sMsg)
{
  Gui().WriteTrace(sMsg);
}

void CCommon::Sleep(int iTime)
{
  IScriptClass::Sleep(iTime);
}

void CCommon::Import(QString sScript)
{
  QJSEngine* pEngine = qjsEngine(this);

  if (QDir::isAbsolutePath(sScript) == false)
  {
    QString sPath = dynamic_cast<ScriptEngine::IScriptEngine*> (pEngine)->GetCurrentScript();
    sScript = sPath.left(sPath.lastIndexOf("/")) + sScript;
  }

  QFile file(sScript);
  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    QTextStream input(&file);
    QString sContent = input.readAll();

    pEngine->evaluate(sContent, sScript);
  }
  else
  {
    Gui().WriteLog("Error including " + sScript.split("/").last());
  }
}

IScriptClass* CCommon::GetInstance(void)
{
  return new CCommon();
}

}
}
