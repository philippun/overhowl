/*
 * Copyright 2018 Philipp Unger
 * philipp.unger.1988@gmail.com
 *
 * This file is part of Overhowl.
 *
 * Overhowl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Overhowl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Overhowl. If not, see <http://www.gnu.org/licenses/>.
 */
#include "CCommonExtension.h"
#include "CCommon.h"


namespace Overhowl {
namespace ScriptEngine {

QList<IScriptClass*> CCommonExtension::GetScriptClasses()
{
  return { new CCommon() };
}

QList<QString> CCommonExtension::GetScriptFunctions()
{
  return {
    "var common = new Common();"
    "function Print(msg) { common.Print(msg); }"
    "function Trace(msg) { common.Trace(msg); }"
    "function Sleep(time) { common.Sleep(time); }"
    "function Import(script) { common.Import(script); }"
  };
}

}
}
